#!/bin/bash
#PBS -q copperhead
#PBS -l nodes=1:ppn=1
#PBS -l mem=32gb
#PBS -l walltime=4:00:00

# supplied by qsub -v options:
#   BED - name of bed file with gene models
#   F - number of bases flanking intron for junction support
#   RIF - number of bases inside intron for retained intron support

module load samtools
cd $PBS_O_WORKDIR
S=$(basename $BED .bed)
S=${S}_${F}_${RIF}

if [ ! -f allJunctions_$S.bed ]; then
    PreParse.py -s _$S *.FJ.bed.gz
fi
if [ ! -f AltSplicing_$S.abt ]; then
    ArabiTagMain.py -g $BED -f $F -s $S -e allJunctions_$S.bed
fi
if [ ! -f AS_$S.txt ]; then
    PostParse.py -b $BED AltSplicing_$S.abt AS_$S.txt
fi
if [ ! -f AS_${S}_wRI.txt ]; then
    Add_RiGp_Support.py -f $RIF -d .. -i AS_$S.txt -o AS_${S}_wRI.txt
fi
