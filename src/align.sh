#!/bin/bash

# run like this: 
# align.sh 1>jobs.out 2>jobs.err
# then if you have to kill the jobs, 
# cat jobs.out | xargs qdel 

SCRIPT=runTophat.sh
D=seedling_TH2.1.1
G=$BOWTIE2_INDEXES/O_sativa_japonica_Oct_2011
FS=`ls fastq/*.fastq.gz`
for F in $FS
do
    S=`basename $F .fastq.gz`
    CMD="qsub -N $S -o $S.out -e $S.err -vSAMPLE=$S,OUTDIR=$D,GENOME=$G,FASTQ=$F $SCRIPT"
    $CMD
done
