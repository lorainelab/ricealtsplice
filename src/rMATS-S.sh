#!/bin/bash
RMATDIR=/lustre/groups/lorainelab/sw/rMATS.3.2.5
S=$RMATDIR/RNASeq-MATS.py
GTF=$RMATDIR/gtf/O_sativa_japonica_Oct_2011.gtf
# prefix
P='Control2h-S'
CONTR="${P}1.bam,${P}2.bam,${P}3.bam"
P='BA2h-S'
TREAT="${P}1.bam,${P}2.bam,${P}3.bam"
DIR=rMATS-S
CMD="python $S -o $DIR -b1 $TREAT -b2 $CONTR -gtf $GTF -len 100 -t single"
echo "running: $CMD" > $DIR.out
$CMD 2>$DIR.err 1>>$DIR.out  
echo "DONE."