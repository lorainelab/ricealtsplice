#!/usr/bin/env python

"""
Build data file classifying difference regions 
"""

import optparse, sys
import Mapping.Parser.Bed as b
import Mapping.Parser.AltSpliceCounts as a
import Mapping.FeatureModel as f
import Utils.General as u

sep = '\t'

def getFeats(fname='../ExternalDataSets/O_sativa_japonica_Oct_2011.bed.gz'):
    feats=[]
    fh=u.readfile(fname)
    while 1:
        line = fh.readline()
        if not line:
            fh.close()
            break
        else:
            feat=b.bedline2feat(line.rstrip())
            feats.append(feat)
    return feats

def getSpliceRegions(fname="../AltSplice/results/seedling_SupportCounts.txt.gz"):
    diff_regions = a.getFeats(fname=fname)
    return diff_regions

def classifyDiffRegions(feats=None,regions=None):
    transcripts={}
    gene2cds={}
    for feat in feats:
        key = feat.getDisplayId()
        if transcripts.has_key(key):
            raise ValueError("Duplicate feature object for: %s",key)
        transcripts[feat.getDisplayId()]=feat
        gene_id=feat.getVal('gene_id')
        if not gene_id:
            gene_id = feat.getVal('symbol')
            if not gene_id:
                raise ValueError("Can't determine gene_id for %s",key)
        cdss=feat.getSortedFeats(feat_type="CDS")
        # some models may not be protein-coding
        if len(cdss)==0:
            continue
        start=cdss[0].getStart() 
        end=cdss[-1].getEnd()
        if gene2cds.has_key(gene_id):
            if start < gene2cds[gene_id][0]:
                gene2cds[gene_id][0]=start
                gene2cds[gene_id][2]=1
            if end > gene2cds[gene_id][1]:
                gene2cds[gene_id][1]=end
                gene2cds[gene_id][2]=1
        else:
            gene2cds[gene_id]=[start,end,0]
    header=['gene','as.id','S','L','chromosome','start','end','len','strand',
            'type','UTR5','coding','UTR3','diff_cds','cds_start','cds_end']
    results=[]
    results.append(header)
    for region in regions:
        gene_id=region.getVal('gene_id')
        vals=[gene_id,region.getDisplayId(),
              region.getVal('S'),region.getVal('L'),region.getSeqname(),
              str(region.getStart()),str(region.getEnd()),
              str(region.getLength()),str(region.getStrand()),
              region.getVal("altsplice_type")]
        if not gene2cds.has_key(gene_id):
            vals=vals+['NA','NA','NA','NA','NA','NA']
        else:
            [start,end,diff_cds]=gene2cds[gene_id]
            cds=f.DNASeqFeature(start=start,length=end-start,
                                                  strand=region.getStrand(),
                                                  seqname=region.getSeqname())
            if region.overlaps(cds,ignore_strand=False):
                vals=vals+['0','1','0',str(diff_cds),str(cds.getStart()),str(cds.getEnd())]
                # sys.stderr.write("coding\n")
            elif region.isFivePrimeOf(cds):
                vals=vals+['1','0','0',str(diff_cds),str(cds.getStart()),str(cds.getEnd())]
                # sys.stderr.write("5'\n")
            elif region.isThreePrimeOf(cds):
                vals=vals+['0','0','1',str(diff_cds),str(cds.getStart()),str(cds.getEnd())]
                # sys.stderr.write("3'\n")
            else:
                sys.stderr.write("CDS:\n%s:%i-%i\n"%(region.getSeqname(),start,end))
                raise ValueError("Can't determine location of alternatively spliced region relative to protein-coding region for: %s in %s"%(region.getDisplayId(),gene_id))
        results.append(vals)
    return results
              
def main(feats=None,regions=None,bed_file=None,splicing_file=None):
    if not feats:
        feats = getFeats(fname=bed_file)
    if not regions:
        regions = getSpliceRegions(fname=splicing_file)
    results = classifyDiffRegions(feats,regions)
    for result in results:
        sys.stdout.write(sep.join(result)+'\n')
    return results

if __name__ == '__main__':
    usage = """
    %prog [options]

    Reads gene model file, alternative splicing file.
    Prints to stdout rows of tab-delimited data.
    Each row represents an alternative splicing choice.
    Data include:
    First 10 columns of alternative splicing file, plus 6 new columns:
    
    5UTR: 1 if region is entirely in 5' UTR, 0 otherwise
    coding: 1 if region overlaps coding region, 0 otherwise
    3UTR: 1 if region is entirely in 3' UTR, 0 otherwise
    diff_cds: 1 if different transcripts from the same gene have different CDS start and end positions
    cds_start: position on chromosome of smallest start of a CDS from transcripts from gene in field 1
    cds_end: position on chromosome of largest end of a CDS from transcripts from gene in field 1

    Coordinates are interbase.
    +1 is plus (top) strand.
    -1 is negative (bottom) strand

    ex) effectsOnProtein.py -s ../AltSplice/results/seedling_SupportCounts.txt.gz
                            -b ../ExternalDataSets/O_sativa_japonica_Oct_2011.bed.gz
                            | gzip > ../AltSplice/results/protein_effects.txt.gz
    """
    parser = optparse.OptionParser(usage=usage)
    parser.add_option('-b','--bed_file',
                      help="bed file to read [required]",
                      dest='bed_file',default=None)
    parser.add_option('-s','--splicing_file',
                      help="alternative splicing file [required]",
                      dest="splicing_file",default=None)
    (options,args)=parser.parse_args()
    bed_file = options.bed_file
    splicing_file = options.splicing_file
    if not bed_file or not splicing_file:
        parser.print_help()
        exit(1)
    main(bed_file=options.bed_file,splicing_file=options.splicing_file)
