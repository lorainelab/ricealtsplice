#!/bin/bash
#B=O_sativa_japonica_Oct_2011.bed.gz
B=MSU7_IRGSP_STRG.NM.bed
S=seedling_all_models
F=5
RI=10
gunzip -c $B.gz | cut -f1-12 > $B
PreParse.py -s _$S *FJ.bed.gz
ArabiTagMain.py -g $B -e allJunctions_$S.bed -s $S -f $F
O=AltSplicing_$S.$F.txt
PostParse.py -b $B AltSplicing_$S.abt AltSplicing_$S.f$F.AS.txt
Add_RiGp_Support.py -o AltSplicing_$S.f$F.ASwiRI.txt -i AltSplicing_$S.f$F.AS.txt -f $RI -d ..