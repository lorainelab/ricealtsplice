#!/bin/bash
module load stringtie
B=All.sm.bam
stringtie All.sm.bam -p 6 -G O_sativa_japonica_Oct_2011.gtf -i -o stringtie.gtf 2>stringtie.err 