#!/usr/bin/env python

# to run this, get git repository https://bitbucket.org/lorainelab/igbquickload
# put igbquickload root directory in your PYTHONPATH

from Quickload import *
import QuickloadUtils as utils
from AnnotsXmlForRNASeq import *

ANNOTSBACKGROUND='FFFFFF'

cr="1F78B4"
cs="33A02C"
tr='9900FF' 
ts='8B2323'

deploy_dir="seedling_TH2.1.1_processed"
genome="O_sativa_japonica_Oct_2011"
u = "%s/%s"%(genome,deploy_dir)


lsts =[['BA2h-R1','Tr Root1',tr,ANNOTSBACKGROUND,u],
         ['BA2h-R2','Tr Root2',tr,ANNOTSBACKGROUND,u],
         ['BA2h-R3','Tr Root3',tr,ANNOTSBACKGROUND,u],
         ['Control2h-R1','Cntrl Root1',cr,ANNOTSBACKGROUND,u],
         ['Control2h-R2','Cntrl Root2',cr,ANNOTSBACKGROUND,u],
         ['Control2h-R3','Cntrl Root3',cr,ANNOTSBACKGROUND,u],
         ['BA2h-S1','Tr Shoot1',ts,ANNOTSBACKGROUND,u],
         ['BA2h-S2','Tr Shoot2',ts,ANNOTSBACKGROUND,u],
         ['BA2h-S3','Tr Shoot3',ts,ANNOTSBACKGROUND,u],
         ['Control2h-S1','Cntrl Shoot1',cs,ANNOTSBACKGROUND,u],
         ['Control2h-S2','Cntrl Shoot2',cs,ANNOTSBACKGROUND,u],
         ['Control2h-S3','Cntrl Shoot3',cs,ANNOTSBACKGROUND,u]]

def makeAnnotsFiles():
    files=[]
    quickload_file = AnnotationFile(path="MSU7_IRGSP_STRG.NM.bed.gz",track_name="ALL gene models",
                                    track_info_url=genome,show2tracks=True,
                                    tool_tip="All gene models, relabeled")
    files.append(quickload_file)
    quickload_file = AnnotationFile(path="stringtie.bed.gz",track_name="Stringtie models",
                                    track_info_url=genome,show2tracks=True,
                                    tool_tip="Made from RNA-Seq data using stringtie")
    files.append(quickload_file)
    quickload_file = AnnotationFile(path="IRGSP-1.0_predicted_2015-03-31.bed.gz",track_name="IRGSP predicted",
                                    track_info_url=genome,show2tracks=True,
                                    tool_tip="Predicted gene models, IRGSP")
    files.append(quickload_file)
    return files
                                    
def makeQuickloadFiles():
    quickload_files = makeAnnotsFiles()
    rnaseq_files = makeQuickloadFilesForRNASeq(lsts=lsts,folder="RNA-Seq/seedling",
                    deploy_dir=deploy_dir)
    for file in rnaseq_files:
        quickload_files.append(file)
    return quickload_files

if __name__=='__main__':
    about="Make annots.xml text for rice alternative splicing study."
    utils.checkForHelp(about)
    quickload_files=makeQuickloadFiles()
    main(quickload_files=quickload_files)
