#!/usr/bin/env python

info="Read BED14 file from stdin.\nWrite gene info to stdout:\n  gene_id [tab] gene description"

import sys,optparse

def main(bed_file=None):
    fh=sys.stdin
    d={}
    while 1:
        line = fh.readline()
        if not line:
            break
        toks=line.rstrip().split('\t')
        transcript_id=toks[3]
        gene_id='.'.join(transcript_id.split('.')[0:-1])
        if not d.has_key(gene_id):
            d[gene_id]=[toks]
        else:
            d[gene_id].append(toks)
    gene_ids = d.keys()
    sys.stdout.write('gene_id\tdescr\n')
    for gene_id in gene_ids:
        tokss=d[gene_id]
        descrs={}
        for toks in tokss:
            descr=toks[-1]
            if not descr=='NA':
                if descr.endswith('.'):
                    descr=descr[0:-1]
                descrs[descr]=descr
        if len(descrs.values())>0:
            descr='; '.join(descrs.keys())
        else:
            descr='NA'
        sys.stdout.write('%s\t%s\n'%(gene_id,descr))


if __name__ == '__main__':
    # using optparse to get -h option 
    usage = "%prog\n\n"+info
    parser = optparse.OptionParser(usage)
    (options,args)=parser.parse_args()
    main()

