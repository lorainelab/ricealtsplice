#!/bin/sh

# thanks to Aaron Quinlan
# https://twitter.com/aaronquinlan/status/715262758451683328
in='All.sm.bam'
out='All.spliced.sm.bam'
samtools view -h $in | awk '$6 ~ /N/ || $0 ~ /^@/' | samtools view -Sb - > $out 
samtools index $out




