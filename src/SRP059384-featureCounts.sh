#!/bin/bash
#PBS -q copperhead
#PBS -l nodes=1:ppn=1
#PBS -l mem=6gb
#PBS -l walltime=8:00:00
#PBS -N SRP059384
cd $PBS_O_WORKDIR
module load subread
# -p data are paired-end
# -C do not count alignments if read pair members are on different chromosomes
# -O count each read for each feaure it overlaps, even if the features
# overlap other features
SAF=TAIR10_SAF_for_featureCount.txt 
BASE=SRP059384_TAIR10
OUT=$BASE.txt
ERR=$BASE.err
OUT2=$BASE.out
# don't include multi-mapping reads
featureCounts -p -O -C -F SAF -a $SAF -o $OUT *.bam 2>$ERR 1>$OUT2
gzip $OUT

