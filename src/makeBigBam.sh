#!/bin/sh

# list BAMs for samples
bams="$(ls *.bam)"
srp="All"
# merge the files, preserve read group
samtools merge -r $srp.bam $bams
samtools index $srp.bam
# isolate single-mapping reads, save to SAM (text) file
samtools view $srp.bam | grep "NH:i:1" > $srp.sam
# get genome.txt file if does not exist
if [ ! -s genome.txt ]; then
    wget http://www.igbquickload.org/quickload/O_sativa_japonica_Oct_2011/genome.txt
fi
# convert SAM to BAM
# this fails a lot - due to missing newlines and other stuff
samtools view -t genome.txt -bS $srp.sam > $srp.sm.bam 2> sam2bam_conversion.err
if [ $? -eq 0 ]; then # did it work? if yes, proceed. If not ... exit
    if [ !-s header.sam ]; then
        samtools view -H $srp.bam > header.sam
    fi
    samtools reheader header.sam $srp.sm.bam > $tmp.bam
    mv $tmp.bam $srp.sm.bam
    samtools index $srp.sm.bam
else
    echo "Failed because of:"
    cat sam2bam_conversion.err
    exit(1)
fi



