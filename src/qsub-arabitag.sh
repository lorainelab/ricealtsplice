#!/bin/bash

# to run: 
#   - change into subdirectory of folder containing BAM files
#   - add links to FJ files, output of Loraine Lab find_junctions
#   - execute:
#        qsub-arabitag.sh 1>jobs.out 2>jobs.err
# 
# To kill jobs:
#   -    cat jobs.out | xargs qdel

BASE=cyto
# gene model files location 
EDS=~/src/hotdryarabidopsis/ExternalDataSets
# min bases flanking a junction
F=5
# min bases overlapping a retained intron
RIF=10
SCRIPT=arabitag.sh

BEDS="TAIR10.bed Araport11.bed AtRTD2-Van.bed"
for BED in $BEDS
do
    S=$(basename $BED .bed)
    gunzip -c $EDS/$BED.gz | cut -f1-12 > $BED
    CMD="qsub -N $S -o $S.out -e $S.err -vBED=$BED,F=$F,RIF=$RIF $SCRIPT"
    $CMD
done


