#!/bin/bash
# To run, I thought you needed python 2.7 or
# better. So I tried this:
#   module load python 
# which loads python 2.7.
# I then installed dependencies:
#   pip install --user numpy scipy
#   pip install --user pysam
# But first, I had to delete a config file for pip - 
# see: http://stackoverflow.com/questions/42304431/pip-install-user-reports-successful-installation-but-nothings-there
# But then, when I tried to run the program, I got an error.
# By accident, I discovered that rMATS was running fine in system default
# python - Python 2.6.6 (r266:84292, Aug 12 2014, 07:57:07) 
# https://twitter.com/ThePracticalDev/status/717357082547208196
RMATDIR=/lustre/groups/lorainelab/sw/rMATS.3.2.5
S=$RMATDIR/RNASeq-MATS.py
GTF=$RMATDIR/gtf/O_sativa_japonica_Oct_2011.gtf
# prefix
P='Control2h-R'
CONTR="${P}1.bam,${P}2.bam,${P}3.bam"
P='Control2h-S'
TREAT="${P}1.bam,${P}2.bam,${P}3.bam"
DIR=rMATS-RvS
CMD="python $S -o $DIR -b1 $CONTR -b2 $TREAT -gtf $GTF -len 100 -t single"
echo "running: $CMD" > $DIR.out
$CMD 2>$DIR.err 1>>$DIR.out  
echo "DONE."