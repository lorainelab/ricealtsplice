#!/bin/bash
module load stringtie
B=All.spliced.sm.bam
out=stringie.splicedPlusModels
stringtie $B -p 6 -G O_sativa_japonica_Oct_2011.gtf -i -o $out.gtf 2>$out.err 
out=stringie.spliced
stringtie $B -p 6 -i -o $out.gtf 2>$out.err 