#!/bin/bash
module load subread
BASE=seedling
OUT=$BASE.txt
ERR=$BASE.err
OUT2=$BASE.out
G=O_sativa_japonica_Oct_2011.gtf
featureCounts -T 6 -a $G -o $OUT *.bam 2>$ERR 1>$OUT2