#!/usr/bin/env python

ex=\
"""

Extract GO terms for MSU gene annotations from Gramene GO file.
Reads from stdin, writes to stdout.

ex)

gunzip -c gene_association.gramene_oryza.gz | %prog > go2gene.txt
"""

import os,sys,optparse,fileinput,re
    
def main(args):
    reg1=re.compile(r'LOC_Os..g\d{5}')
    d= {}
    for line in fileinput.input(args):
        genes=reg1.findall(line)
        if len(genes)>0:
            toks=line.rstrip().split('\t')
            GO=toks[4]
            ontology=toks[8]
            for gene in genes:
                if not d.has_key(ontology):
                    d[ontology]={}
                if not d[ontology].has_key(GO):
                    d[ontology][GO]={}
                d[ontology][GO][gene]=1
    sys.stdout.write("GO\tOntology\tgene\n")
    for ontology in d.keys():
        for GO in d[ontology].keys():
            for gene in d[ontology][GO].values():
                sys.stdout.write('%s\t%s\t%s\n'%(GO,ontology,gene))

if __name__ == '__main__':
    usage = "%prog"+ex
    parser = optparse.OptionParser(usage)
    (options,args)=parser.parse_args()
    main(args)

# this was useful
# http://stackoverflow.com/questions/9756396/remove-parsed-options-and-their-values-from-sys-argv
