# Many rice genes are differentially spliced between roots and shoots but cytokinin has minimal effect on splicing.

An article describing this work appears in the on-line journal Plant Direct. 

* * *

## About this repository

This repository contains analysis folders named for the type of data
analysis code and results they contain.

Most analysis folders contain:

* .Rproj file - an RStudio project file. Open this in RStudio to re-run project code.
* .Rmd files - R Markdown files that contain an analysis focused on answering one or two discrete questions. These are typically formatted like miniature research articles, with Introduction, Results/Analysis, and Conclusions sections. 
* .html files - output from running knitr on .Rmd files. 
* results folders - contain files (typically tab-delimited) and images produced by .Rmd files. 
* data - data files obtained from external sources
* src - R, python, or perl scripts used for simple data processing tasks
* README.md - documentation 

Some modules depend on the output of other modules. Some modules also
depend on externally supplied data files, which are typically version-controlled
in ExternalDataSets but may also be available from external sites.

* * *

## What's here 

* * *

### AltSplice 

Contains analysis of splicing patterns. Look here for output of the ArabiTag software, used to identify and classify alternatively spliced and differentially spliced regions. 

* * *

### Expression

Contains read counts per gene determined by featureCounts. Contains code for generating normalized expression values per gene.

* * *

### Experimental Testing

Results from fragment analysis of alternative splicing. 

* * *

### ExternalDataSets

Contains gene annotations and other data sets downloaded from IGBQuickLoad.org, GeneOntology.org, and other sources.

* * *

### GeneModelConsolidation

An attempt to combine gene models from various sources into one annotation data set. Not used at this time. (We may come back to it later depending on time and resources.)

* * *

### Junctions

Analyzing splice junctions and their properties. 

* * *

### Manuscript 

Figures for publication. 

* * *

### src

Contains bash and python scripts for running small tasks. Contains R program files with project-wide functions and variables. 

* * *

### Test

Contains test files (BAM, IGB screen shots, etc) for battle-testing gene model consolidation and other algorithms for analysis of splicing patterns.

* * *

## License 

Copyright (c) University of North Carolina at Charlotte

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Also, see: http://opensource.org/licenses/MIT