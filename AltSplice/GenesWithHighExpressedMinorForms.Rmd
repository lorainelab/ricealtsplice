---
title: "What kinds of genes have high expressed minor forms?"
author: "Ann Loraine"
date: "February 23, 2018"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Introduction

Around 30% of alternative splicing events had at least 20% read support for the minor form.

What types of genes are these?

## Analysis

Read data and identify AS events where the minor form is supported by 20% or more of overlapping reads:

```{r}
fname="results/totalCounts.txt"
d=read.csv(fname,sep="\t",quote="",as.is=T)
total=d$L+d$S
mf_support=ifelse(d$L<d$S,d$L,d$S)
mf=(mf_support/total)*100
d$mf=mf
MF=20
coverage=10
indexes=total>=coverage&mf>MF
```

Examine genes sorted by minor form abundance with at least `r coverage` coverage over differentially spliced regions:

```{r}
mfs=d[indexes,]
o=order(mfs$mf,decreasing = T)
mfs=mfs[o,]
```

There were `r length(unique(mfs$gene))` genes where the minor form frequency was 20% or larger. This was `r round(100*length(unique(mfs$gene))/length(unique(d[(d$L+d$S)>coverage,]$gene)),1)`% of all alternatively spliced genes with AS region coverage >= `r coverage` reads. 

Is there enrichment of functional categories in this group of genes relative to all genes in the genome?

```{r}
fname='../ExternalDataSets/gene_association.gramene_oryza.gz'
cmd=paste("gunzip -c",fname,"| ../src/getGOAnnotations.py")
gene2go=read.delim(pipe(cmd),sep='\t',head=F,as.is=T,
                   col.names=c('GO_id','ontology','gene'),na.strings='')
v=paste(gene2go$GO_id,gene2go$gene)
v=!duplicated(v)
gene2go=gene2go[v,]
gene2go=gene2go[,c('gene','GO_id')]
names(gene2go)=c('Gene','category')
gene2go_alt=read.delim(file = "../ExternalDataSets/GOtermsFull.txt.gz",as.is = T)
v=paste(gene2go_alt$Gene,gene2go_alt$category)
v=!duplicated(v)
gene2go_alt=gene2go_alt[v,]
```

Load goseq library:

```{r}
suppressPackageStartupMessages(library(goseq))
```

Function for analyzing GO term enrichment. Arguments:

* gene.vector - vector, 1 or 0 indicating in or out group 
* sizes - vector with transcript sizes, in kb
* gene2go - genes mapped to GO terms

```{r}
do.go=function(gene.vector=NULL,
               sizes=NULL,
               gene2go=NULL) {
  pwf<-nullp(gene.vector,bias.data=sizes,plot.fit=T)
  GO=goseq(pwf,gene2cat=gene2go,method="Hypergeometric")
  o=order(GO$over_represented_pvalue,decreasing=F)
  GO=GO[o,c('category','numDEInCat',
            'numInCat','over_represented_pvalue',
            'under_represented_pvalue','term')]
  GO=GO[!is.na(GO$term),]
  GO
}
```

Read transcript sizes, removing genes from "Sy" and "Un" unassembled chromosomes which are poorly annotated. GO annotations do not include these.

```{r}
fname='../ExternalDataSets/tx_size.msu7.txt.gz'
sizes_df=read.delim(fname,header=T,sep='\t')
sizes=sizes_df$bp/1000
names(sizes)=sizes_df$locus
to_remove=grep('Sy',names(sizes))
to_remove = union(to_remove,grep('Un',names(sizes)))
sizes=sizes[-to_remove]
```


```{r}
gene.vector=rep(0,length(sizes))
names(gene.vector)=names(sizes)
gene.vector[mfs$gene]=1
GO=do.go(gene.vector=gene.vector,sizes=sizes,
         gene2go=gene2go_alt)
GO$over.fdr=p.adjust(GO$over_represented_pvalue,
                     method='BH')
GO$under.fdr=p.adjust(GO$under_represented_pvalue,
                      method='BH')
go_FDR=0.01
sigsover=which(GO$over.fdr<=go_FDR)
sigsunder=which(GO$under.fdr<=go_FDR)
sigs=union(sigsover,sigsunder)
GO$percent=round(GO$numDEInCat/GO$numInCat*100,1)
newGO=GO[sigs,c('category','numDEInCat','numInCat','percent',
                'over.fdr','under.fdr','term')]
```

Number of GO categories with Q (FDR) `r go_FDR` or smaller: `r nrow(newGO)`.

## Conclusion

There is not a significant enrichment of categories among alternatively spliced genes at least 20% overlapping read support for the minor form.

Thus the types of genes were diverse, with no over-represented categories of genes. 
