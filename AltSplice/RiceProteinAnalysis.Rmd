---
title: "Effects of alternative splicing on protein sequence"
output: html_document
---

# Introduction

Alternative splicing can change protein sequence when the alternatively spliced region is located in the protein-coding region of a gene.

Here we investigate how alternative splicing affects protein sequence in rice gene model annotations.

Questions:

* How many genes contained alternatively spliced regions in the coding region? 
* How many AS events changed protein sequence?
* How many alternatively spliced regions were evenly divisible by three and thus were unlikely to produce trucated proteins?
* How many differentially spliced AS events affected protein sequence?

# Analysis

Read data file produced by python script effectsOnProtein.py:

```{r}
d=read.table("results/protein_effects.txt.gz",as.is=T,sep="\t",header=T)
```

There were `r length(unique(d$as.id))` unique AS events.

* How many genes contained alternatively spliced regions in the coding region? 

To answer, make a matrix where rows are genes and columns indicate whether the gene has an AS event affecting the 5-prime UTR, the coding region, and/or the 3-prime UTR. 

For example, a row with values 1, 1, and 1 contains at least three alternative splicing events affecting all three parts of the gene.

```{r}
genes=unique(d$gene)
d.byGene = matrix(0,nrow = length(genes),ncol=3)
row.names(d.byGene)=genes
colnames(d.byGene)=c('UTR5','coding','UTR3')
for (gene in genes) {
  rows=d[d$gene==gene,c('UTR5','coding','UTR3')]
  d.byGene[gene,]=ifelse(colSums(rows)>0,1,0)
}
```

Visualize the results using a 3-part Venn diagram:

```{r, fig.height=5, fig.width=5, message=FALSE, warning=FALSE}
library(limma)
UTR3=(d.byGene[,'UTR3']==1)
UTR5=(d.byGene[,'UTR5']==1)
coding=(d.byGene[,'coding']==1)
v=cbind(UTR5,coding,UTR3)
a=vennCounts(v)
vennDiagram(a)
```

There were `r sum(coding)` genes out of `r length(genes)` alternatively spliced genes that contained AS events in the coding region. This was `r round(sum(coding)/length(genes)*100,1)`% of alternatively spliced genes.

* Did alternative splicing introduce frameshifts more or less often that would be expected by chance?

First, examine all AS events regardless of expression level:

```{r}
coding = d[d$coding==1,]
mod3=coding$len%%3
table(mod3)
```

Determine how unlikely it is for the number of regions divisible by three to be as large as this assuming a null hypothesis that true probability of a region being divisible by three is 1 in 3. 

```{r}
successes=table(mod3)[1]
trials=nrow(coding)
result=binom.test(x=successes,n=trials,p=1/3,
                  alternative = "greater")
```

The p value resulting from the binomial test is `r result$p.value`.

Compare to what happens in the UTRs:

```{r}
UTR=d[d$UTR5==1|d$UTR3==1,]
mod3=UTR$len%%3
successes=table(mod3)[1]
trials=nrow(coding)
result=binom.test(x=successes,n=trials,p=1/3,
                  alternative = "greater")
table(mod3)
```

The p value resulting from the binomial test is `r result$p.value`.

Conlusion: Among alternatively spliced regions that impinge on coding regions, there is a large enrichment of region of that are divisible by three. There is no such enrichment among alternatively spliced regions that map to non-coding regions, however.

The pattern of peaks in the following plots illustrate this enrichment:

```{r fig.width=10,fig.height=4}
main="Sizes of alternatively spliced regions in coding regions"
hist(coding[coding$len<150,]$len,breaks=1:150,xlab="Alternatively spliced region length (bp)",main=main)
```

Values evenly divisible by three are generally taller than their neighbors.

```{r fig.width=10,fig.height=4}
main="Sizes of alternatively spliced regions in UTRs"
hist(UTR[UTR$len<150,]$len,breaks=1:150,xlab="Alternatively spliced region length (bp)",main=main)
```

In the preceding plot, the peaks occur in a more random order.

* Do AS events in the middle part of the PSI distribution change protein sequence?

```{r}
threshold=15
fname=paste0('../AltSplice/results/PSI-',threshold,'.txt')
psi=read.delim2(fname,header=T,sep='\t',as.is = T)
v=rep(FALSE,nrow(psi))
x=0
y=100
for (i in 1:nrow(psi)) {
  for (j in (ncol(psi)-3):ncol(psi)) {
    if (!is.na(psi[i,j]) & psi[i,j]>x & psi[i,j]<y) {
      v[i]=TRUE
      break
    }
  }
}
```

There were `r sum(v)` splicing events with greater than `r x`% minor variant frequency in at least one of the four sample types. This was `r round(sum(v)/length(v)*100,1)`% of AS events.

How many of these were in the coding region of genes?

```{r}
as.ids=psi[v,'as.id']
x=which(as.ids%in%coding$as.id)
y=which(as.ids%in%UTR$as.id)
```

There were `r length(x)` higher-frequency alternatively spliced regions in coding regions and `r length(y)` higher-frequency alternatively spliced regions in non-coding regions.

Of these, how many introduced frameshifts? 

```{r}
mod3.coding=psi[x,]$len%%3
table(mod3.coding)
successes=table(mod3.coding)[1]
trials=length(mod3.coding)
result.coding=binom.test(x=successes,n=trials,p=1/3,
                    alternative = "greater")
mod3.UTR=psi[y,]$len%%3
table(mod3.UTR)
successes=table(mod3.UTR)[1]
trials=length(mod3.UTR)
result.UTR=binom.test(x=successes,n=trials,p=1/3,
                    alternative = "greater")
```

There is a striking enrichment of non-frameshift introducing alternative splicing in coding region and a slight enrichment in UTR higher-minor-frequency alternative splicing events - pvalue `r result.coding$p.value` for coding regions and `r result.UTR$p.value` for UTRs.

* How do differentially spliced regions affect protein sequence?

Read differential splicing results:

```{r}
fname="results/rootsVshoots_MSU7_15.txt.gz"
diffsplice=read.delim2(fname,as.is=T,header=T,quote="")
```

How many of these were in the coding region of genes?

```{r}
v=which(diffsplice$fdr<=0.1)
as.ids=diffsplice[v,'as.id']
x=which(as.ids%in%coding$as.id)
y=which(as.ids%in%UTR$as.id)
```

There were `r length(x)` differentially spliced regions in coding regions and `r length(y)` differentially spliced spliced regions in non-coding regions.

Of these, how many introduced frameshifts? 

```{r}
mod3.coding=diffsplice[x,]$len%%3
table(mod3.coding)
successes=table(mod3.coding)[1]
trials=length(mod3.coding)
result.coding=binom.test(x=successes,n=trials,p=1/3,
                    alternative = "greater")
mod3.UTR=diffsplice[y,]$len%%3
successes=table(mod3.UTR)[1]
trials=length(mod3.UTR)
result.UTR=binom.test(x=successes,n=trials,p=1/3,
                    alternative = "greater")
table(mod3.UTR)
```

There is a slight enrichment of non-frameshift introducing alternative splicing in coding region but not UTR differentially spliced alternative splicing events - pvalue `r result.coding$p.value` for coding regions and `r result.UTR$p.value` for UTRs.

# Conclusions

* Alternative splicing occurs in the coding regions of around 75% (the majority of) alternatively spliced genes in rice.
* Coding region alternative splicing tends to not disrupt reading frame - more so than UTR alternative splicing
* This was true both for AS events in general as well as AS events with higher minor variant frequencies and AS events that were differentially spliced between rice roots and shoots.