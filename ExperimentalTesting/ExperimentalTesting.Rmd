---
title: "Differential splicing between roots and shoots in rice"
date: "February 13, 2017"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Introduction

We used the [arabitag algorithm](https://www.ncbi.nlm.nih.gov/pubmed/20525311) to analyze RNA-Seq data from rice and identify genes that were differentially spliced between rice roots and shoots. Data were from an article titled "[Characterization of the cytokinin-responsive transcriptome in rice](https://www.ncbi.nlm.nih.gov/pubmed/27931185)". We prepared new RNA samples from rice seedlings grown under conditions similar to those used to prepare samples for RNA-Seq. 

We then asked whether genes identified as alterantively spliced using RNA-Seq data were also alternatively spliced in the new samples. To assay splicing in the new samples, we used a method based on fragment analysis of amplified cDNA. This method uses fluor-tagged PCR primers, an ABI sequencer, and capillary gel electrophoresis to separate and quantify RT-PCR products amplified from different splice variants. The proportion of amplicon obtained from each splice variant indicates the relative abundance of splice variants in the original sample. The method and rationale are described in Chapter 23 of [Alternative pre-mRNA Splicing: Theory and Protocols](http://www.wiley.com/WileyCDA/WileyTitle/productCd-3527326065.html).

Here, we analyze the results from these assays and compare them to arabitag analysis of RNA-Seq data.

**Questions:**

* Which of the following genes, if any, were differentially spliced?
* What can these results tell us about the arabitag algorithm? 

**Genes:**

```{r, include=FALSE}
N=10
less_than=0
same_as=0
greater_than=0
```

* LOC_Os01g25484 - ferrodoxin nitrite reductase
* LOC_Os01g35580 - UNK (unknown, expressed protein)
* LOC_Os01g45274 - carbonic anhydrase
* LOC_Os01g51290 - protein kinase
* LOC_Os03g05390 - citrate transporter
* LOC_Os12g08260 - dehydrogenase E1
* LOC_Os01g61670 - ureidoglycolate hydrolase
* LOC_Os05g48040 - MATE efflux family protein
* LOC_Os02g05830 - ribulose bisphosphate carboxylase
* LOC_Os06g05110 - superoxide dismutase

Data are from spreadsheets in a subdirectory named "ABI". Data were copied manually from these spreadsheets into this document. Data are the percentage of signal detected from the so-called "long isoform" (%L), which is the splice variant that includes the differentially spliced region.

Root and shoot samples came from the same pot and thus were paired. Genes and alternatively spliced regions were selected based on results from arabitag analysis of RNA-Seq data. For each alternatively spliced region, we know the sign of the difference between root and shoot %L. To test differential splicing using fragment analysis (ABI) data, we'll do paired, one-sided t-tests. Positive difference of means indicate roots had larger %L.

## Analysis

Obtain normalized average expression data for all the genes:

```{r}
rpkm=read.delim2("../Expression/results/aveRPKM.txt.gz",
                 header=T,sep="\t",row.names = "gene_id",
                 as.is=T,quote = '')[,c("CR","CS")]
rpkm$CS=as.numeric(rpkm$CS)
rpkm$CR=as.numeric(rpkm$CR)
```

### LOC_Os01g25484 - ferrodoxin nitrite reductase

Testing differentially spliced region
Chr1:14452426-14452519 1 RI.

Long form is LOC_Os01g25484.1.

Arabitag pipeline average %L:

```{r}
gene_id="LOC_Os01g25484"
r=74.2
s=31.7
```

The Arabitag pipeline found that %L for roots was `r r`% and in shoots was `r s`%, a difference of `r r-s`%.

```{r}
samples=c("A1","A2","B1","C1","D2","D3","E3")
shoot=c(12.84,11.46,12.37,17.71,10.21,11.67,14.83)
root=c(26.67,29.77,26.30,26.28,32.40,32.48,35.43)
d=data.frame(root,shoot)
row.names(d)=samples
result=t.test(d$root,d$shoot,paired=T,ifelse(r>s,"greater","less"))
result
```

Average %L in roots was `r round(mean(d$root),1)` with standard error `r round(sd(d$root)/sqrt(nrow(d)),2)`%.

Average %L in shoots was `r round(mean(d$shoot),1)` with standard error `r round(sd(d$shoot)/sqrt(nrow(d)),1)`%.

The %L values from fragment analysis were smaller than from RNA-Seq. The statistical test found that the difference in splicing between roots and shoots was highly significant.

```{r, include=FALSE}
less_than=less_than+2
```

Expression values (RPKM):

* Root: `r signif(rpkm[gene_id,'CR'],3)`
* Shoot: `r signif(rpkm[gene_id,'CS'],3)`

### LOC_Os01g35580 - UNK (unknown, expressed protein)

Testing differentially spliced region Chr1:19685602-19685988 1 AS.

Long form(s) are LOC_Os01g35580.8|4|2. 

Arabitag pipeline average %L:

```{r}
gene_id="LOC_Os01g35580"
r=44
s=66.3
```

The Arabitag pipeline found that %L for roots was `r r`% and in shoots was `r s`%, a difference of `r r-s`%.

```{r}
samples=c("A1","A2","B1","C1","D2","D3","E3")
shoot=c(69.20,67.74,69.72,73.45,61.53,61.19,65.34)
root=c(47.14,47.86,47.63,49.22,51.99,52.66,52.97)
d=data.frame(root,shoot)
row.names(d)=samples
result=t.test(d$root,d$shoot,paired=T,ifelse(r>s,"greater","less"))
result
```

Average %L in roots was `r round(mean(d$root),1)` with standard error `r round(sd(d$root)/sqrt(nrow(d)),2)`%.

Average %L in shoots was `r round(mean(d$shoot),1)` with standard error `r round(sd(d$shoot)/sqrt(nrow(d)),1)`%.

The %L values from fragment analysis were very close to those obtained from RNA-Seq. The statistical test found that the difference in splicing between roots and shoots was highly significant.

```{r, include=FALSE}
same_as=same_as+2
```

Expression values (RPKM):

* Root: `r signif(rpkm[gene_id,'CR'],3)`
* Shoot: `r signif(rpkm[gene_id,'CS'],3)`

### LOC_Os01g45274 - carbonic anhydrase

Testing differentially spliced region Chr1:25700121-25700396 1 DS/ES.

Long form is LOC_Os01g45274.3.

Arabitag pipeline average %L:

```{r}
gene_id="LOC_Os01g45274"
r=96.8
s=24.3
```

The Arabitag pipeline found that %L for roots (intron retention) was `r r`% and in shoots was `r s`%, a difference of `r r-s`%.

```{r}
samples=c("A1","A2","B1","C1","D2","D3","E3")
root=c(92.09,98.59,97.42,93.81,100.00,100.00,100.00)
shoot=c(16.83,15.29,13.27,17.22,12.19,16.68,16.30)
d=data.frame(root,shoot)
row.names(d)=samples
result=t.test(d$root,d$shoot,paired=T,ifelse(r>s,"greater","less"))
result
```

Average %L in roots was `r round(mean(d$root),1)` with standard error `r round(sd(d$root)/sqrt(nrow(d)),2)`%.

Average %L in shoots was `r round(mean(d$shoot),1)` with standard error `r round(sd(d$shoot)/sqrt(nrow(d)),2)`%.

The %L values from fragment analysis were very similar to those obtained from RNA-seq for the root samples, but smaller in comparison to the arabitag results for the shoot samples. The statistical test found that the difference in splicing between roots and shoots was significant.

 
```{r, include=FALSE}
same_as=same_as+1
less_than=less_than+1
```

Expression values (RPKM):

* Root: `r signif(rpkm[gene_id,'CR'],3)`
* Shoot: `r signif(rpkm[gene_id,'CS'],3)`

### LOC_Os01g51290 - protein kinase

Testing differentially spliced region Chr1:29488101-29488160 -1 RI

Long form is LOC_Os01g51290.2.

Arabitag pipeline average %L:

```{r}
gene_id="LOC_Os01g51290"
r=88.4
s=95.1
```

The Arabitag pipeline found that %L for roots was `r r`% and in shoots was `r s`%, a difference of `r r-s`%.

```{r}
samples=c("A1","A2","B1","C1","D2","D3","E3")
shoot=c(21.98,22.10,21.61,12.85,17.34,13.52,10.76)
root=c(15.37,10.55,15.82,8.55,16.07,14.44,12.27)
d=data.frame(root,shoot)
row.names(d)=samples
result=t.test(d$root,d$shoot,paired=T,ifelse(r>s,"greater","less"))
result
```

Average %L in roots was `r round(mean(d$root),1)` with standard error `r round(sd(d$root)/sqrt(nrow(d)),2)`%.

Average %L in shoots was `r round(mean(d$shoot),1)` with standard error `r round(sd(d$shoot)/sqrt(nrow(d)),1)`%.

The %L values from fragment analysis were smaller than those obtained from RNA-Seq. The statistical test found that the difference in splicing between roots and shoots was significant.

```{r, include=FALSE}
less_than=less_than+2
```

Expression values (RPKM):

* Root: `r signif(rpkm[gene_id,'CR'],3)`
* Shoot: `r signif(rpkm[gene_id,'CS'],3)`

### LOC_Os03g05390 - citrate transporter

Testing differentially spliced region Chr3:2654271-2654367 -1 RI.

Long form(s) are LOC_Os03g05390.1-6/13.

Arabitag pipeline %L:

```{r}
gene_id="LOC_Os03g05390"
r=86.0
s=96.3
```

The Arabitag pipeline found that %L for roots (intron retention) was `r r`% and in shoots was `r s`%, a difference of `r r-s`%.

```{r}
samples=c("A1","A2","B1","C1","D2","D3","E3")
root=c(88.21,86.74,88.10,88.36,82.90,83.98,83.53)
shoot=c(94.01,95.99,94.57,95.19,95.54,96.48,96.86)
d=data.frame(root,shoot)
row.names(d)=samples
result=t.test(d$root,d$shoot,paired=T,ifelse(r>s,"greater","less"))
result
```

Average %L in roots was `r round(mean(d$root),1)` with standard error `r round(sd(d$root)/sqrt(nrow(d)),2)`%.

Average %L in shoots was `r round(mean(d$shoot),1)` with standard error `r round(sd(d$shoot)/sqrt(nrow(d)),1)`%.

The %L values from fragment analysis were almost identical to those obtained from RNA-Seq. The statistical test found that the difference in splicing between roots and shoots was highly significant.

```{r, include=FALSE}
same_as=same_as+2
```

Expression values (RPKM):

* Root: `r signif(rpkm[gene_id,'CR'],3)`
* Shoot: `r signif(rpkm[gene_id,'CS'],3)`

### LOC_Os12g08260 - dehydrogenase E1

Testing differentially spliced region
Chr12:4212667-4212754 1 RI

Long form is LOC_Os12g08260.5.

Arabitag %L:

```{r}
gene_id="LOC_Os12g08260"
r=55.7
s=2.9
```

The Arabitag pipeline found that %L for roots (intron retention) was `r r`% and in shoots was `r s`%, a difference of `r r-s`%.

```{r}
samples=c("A1","A2","B1","C1","D2","D3","E3")
root=c(4.34,5.45,4.49,3.96,4.03,3.06,3.71)
shoot=c(0.72,0.53,1.10,1.12,0.69,0.91,0.99)
d=data.frame(root,shoot)
row.names(d)=samples
result=t.test(d$root,d$shoot,paired=T,ifelse(r>s,"greater","less"))
result
```

Average %L in roots was `r round(mean(d$root),1)` with standard error `r round(sd(d$root)/sqrt(nrow(d)),2)`%.

Average %L in shoots was `r round(mean(d$shoot),2)` with standard error `r round(sd(d$shoot)/sqrt(nrow(d)),2)`%.

The %L values from fragment analysis of root samples were less than those obtained from RNA-Seq. The %L obtained from fragment analysis of shoot samples were very similar. The statistical test found that the difference in splicing between roots and shoots was highly significant.

```{r, include=FALSE}
same_as=same_as+1
less_than=less_than+1
```

Expression values (RPKM):

* Root: `r signif(rpkm[gene_id,'CR'],3)`
* Shoot: `r signif(rpkm[gene_id,'CS'],3)`

### LOC_Os01g61670 - ureidoglycolate hydrolase

Testing differentially spliced region 
Chr1:35682659-35683067 1 DS

Long form is LOC_Os01g61670.2.

Arabitag %L:

```{r}
gene_id="LOC_Os01g61670"
r=78.0
s=31.0
```

The Arabitag pipeline found that %L for roots (intron retention) was `r r`% and in shoots was `r s`%, a difference of `r r-s`%.

```{r}
samples=c("A1","A2","B1","C1","D2","D3","E3")
root=c(90.08,90.53,88.74,90.07,93.29,92.91,92.79)
shoot=c(39.41,40.59,37.04,37.71,42.44,42.16,35.14)
d=data.frame(root,shoot)
row.names(d)=samples
result=t.test(d$root,d$shoot,paired=T,ifelse(r>s,"greater","less"))
result
```
Average %L in roots was `r round(mean(d$root),1)` with standard error `r round(sd(d$root)/sqrt(nrow(d)),2)`%.

Average %L in shoots was `r round(mean(d$shoot),2)` with standard error `r round(sd(d$shoot)/sqrt(nrow(d)),2)`%.

The %L values from fragment analysis were greater than those obtained from RNA-Seq. The statistical test found that the difference in splicing between roots and shoots was significant.

```{r, include=FALSE}
greater_than=greater_than+2
```

Expression values (RPKM):

* Root: `r signif(rpkm[gene_id,'CR'],3)`
* Shoot: `r signif(rpkm[gene_id,'CS'],3)`

### LOC_Os05g48040 - MATE efflux family protein

Testing differentially spliced region 
Chr5:27544440-27544973 1 DS

Long form is LOC_Os05g48040.2.

The gene contains an alternatively spliced intron with two alternative donor sites. 


Arabitag %L:

```{r}
gene_id="LOC_Os05g48040"
r=88.1
s=100.0
```

The Arabitag pipeline found that %L for roots was `r r`% and in shoots was `r s`%, a difference of `r r-s`%.



```{r}
samples=c("A1","A2","B1","C1","D2","D3","E3")
root=c(93.86,93.10,93.34,91.93,81.30,
       80.48,91.75)
shoot=c(86.77,89.47,81.45,84.92,89.02,
        91.07,95.16)
d=data.frame(root,shoot)
row.names(d)=samples
result=t.test(d$root,d$shoot,paired=T,ifelse(r>s,"greater","less"))
result
```
Average %L in roots was `r round(mean(d$root),1)` with standard error `r round(sd(d$root)/sqrt(nrow(d)),2)`%.

Average %L in shoots was `r round(mean(d$shoot),2)` with standard error `r round(sd(d$shoot)/sqrt(nrow(d)),2)`%.

The %L values from fragment analysis of shoot samples were less than those obtained from RNA-Seq. The %L obtained from fragment analysis of root samples were very similar. For this gene assay, the statistical test found that the difference in splicing between roots and shoots was not significant. However it should be noted that this was a alternative donor event in which there were only a handful of reads (between 2 and 6) that supported the shorter form in the root samples and none in the shoot samples. This event may have been detected as being statistically significant, but the degree of difference appears to be very small considering the low read coverage for these two variants. 

```{r, include=FALSE}
same_as=same_as+1
less_than=less_than+1
```

Expression values (RPKM):

* Root: `r signif(rpkm[gene_id,'CR'],3)`
* Shoot: `r signif(rpkm[gene_id,'CS'],3)`

### LOC_Os02g05830 - ribulose bisphosphate carboxylase

Testing differentially spliced region
Chr2:2875534-2875679 1 RI

Long form is LOC_Os02g05830.2. 

Arabitag %L:

```{r}
gene_id="LOC_Os02g05830"
r=88.2
s=10.1
```
The Arabitag pipeline found that %L for roots (intron retention) was `r r`% and in shoots was `r s`%, a difference of `r r-s`%.
```{r}
samples=c("A1","A2","B1","C1","D2","D3","E3")
root=c(9.90,40.42,28.96,19.95,14.83,26.97,26.34)
shoot=c(2.59,2.60,3.38,4.46,3.21,2.24,2.95)
d=data.frame(root,shoot)
row.names(d)=samples
result=t.test(d$root,d$shoot,paired=T,ifelse(r>s,"greater","less"))
result
```
Average %L in roots was `r round(mean(d$root),1)` with standard error `r round(sd(d$root)/sqrt(nrow(d)),2)`%.

Average %L in shoots was `r round(mean(d$shoot),2)` with standard error `r round(sd(d$shoot)/sqrt(nrow(d)),2)`%.

The %L values from fragment analysis of both root and shoot samples were less than those obtained from RNA-Seq. The statistical test found that the difference in splicing between roots and shoots was significant.

```{r, include=FALSE}
less_than=less_than+2
```


Expression values (RPKM):

* Root: `r signif(rpkm[gene_id,'CR'],3)`
* Shoot: `r signif(rpkm[gene_id,'CS'],3)`

### LOC_Os06g05110 - superoxide dismutase

Testing differentially spliced region 
Chr6:2258569-2258718 1 RI

Long form is LOC_Os06g05110.3.

Arabitag %L:

```{r}
gene_id="LOC_Os06g05110"
r=38.5
s=13.6
```

The Arabitag pipeline found that %L for roots (intron retention) was `r r`% and in shoots was `r s`%, a difference of `r r-s`%.

```{r}
samples=c("A1","A2","B1","C1","D2","D3","E3")
root=c(20.35,22.63,22.65,21.60,21.47,22.52,27.57)
shoot=c(6.48,6.34,5.94,6.39,5.38,6.86,8.13)
d=data.frame(root,shoot)
row.names(d)=samples
result=t.test(d$root,d$shoot,paired=T,ifelse(r>s,"greater","less"))
result
```
Average %L in roots was `r round(mean(d$root),1)` with standard error `r round(sd(d$root)/sqrt(nrow(d)),2)`%.

Average %L in shoots was `r round(mean(d$shoot),2)` with standard error `r round(sd(d$shoot)/sqrt(nrow(d)),2)`%.

The %L values from fragment analysis of both root and shoot samples were less than those obtained from RNA-Seq. The statistical test found that the difference in splicing between roots and shoots was significant.

```{r, include=FALSE}
less_than=less_than+2
```

Expression values (RPKM):

* Root: `r signif(rpkm[gene_id,'CR'],3)`
* Shoot: `r signif(rpkm[gene_id,'CS'],3)`

## Conclusion

For each gene except LOC_Os05g48040, fragment analysis confirmed differential splicing. However, %L values obtained from fragment analysis were often less than those calculated from RNA-Seq. Out of `r N*2` fragment analysis %L measurements, `r less_than` were less than their RNA-Seq counterparts. 

## Discussion

To assess support for a retained intron choice, Arabitag counts the number of reads that align within the intron. It assesses support for the intron-removed form using gapped (spliced) reads that align across the intron. Because the retained-intron form contains more sequence from which a retained intron-derived sequence can be sampled, support for the retained-intron form is easier to obtain. Likewise, because the fragment analysis method involves PCR amplification of splice variants, there may be bias against the RI forms, because they are longer. Bigger size differences may exacerbate these effects.  

However, these biases are inherent to the gene structure itself. This means that these biases would affect different samples equally. Thus, both methods can detect when splicing abundances have changed. 

We also examined the relationship between %L differences 
between RNA-Seq and ABI-based splicing assay measurements. 
The splicing assay includes an ampflication step that could skew results if the amplification exited the logarithmic phase. This could happen if expression was very high. However, there was no relationship between the RNA-Seq versus ABI assay
discrepency and expression level. Thus PCR amplification 
artifacts could not account for the disagreement between
qPCR and RNA-Seq.



## Limitations

The study lacked negative controls - examples of testing alternative splicing where there was no differential splicing. 