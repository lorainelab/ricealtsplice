## Database:

fasta of Reviewed (550,740) Swiss-Prot downloaded from [http://www.uniprot.org/downloads](http://www.uniprot.org/downloads)  with release-3/17/16

## Tool:

BLAST 2.2.26+  with paramters -max_target_seqs 1 -outfmt 6 -evalue 1e-5

## Instruction:

There are two ways to run blastp. We recommand the first solution, it's much faster. Be careful, we found the blastp result is inconsistent between two solutions. In theory, they should be same. We didn't find the reason, but it only effected very tiny amout of queries's annotation. 99.98% queries have the same annotation from two solutions.

**Solution 1:**

Firstly you split fasta sequences of queries into 300 files. Files will only be broken at fa record boundaries. Then you generate the bash file for submitting jobs parallelly.

In terminal, type following command,

<pre><code>cd intermediateFiles
rm -rf longest_orfs.pep.split
mkdir -p longest_orfs.pep.split
cd longest_orfs.pep.split
../../src/faSplit sequence ../stringtie.exons.fa.transdecoder_dir/longest_orfs.pep 300 orf_
cd ..

perl -e '@a=glob("/home/jjduan/06ricesplicing/ricealtsplice/GeneModelConsolidation/intermediateFiles/longest_orfs.pep.split/orf*fa");print "#!/bin/bash\n#SBATCH --partition normal\n#SBATCH --mem-per-cpu 16G\n#SBATCH -c 2\n#SBATCH -t 240:00:00\n\n\n";foreach $i (@a){print "source /com/extra/BLAST/LATEST/load.sh;blastp -query $i -db /home/jjduan/06ricesplicing/ricealtsplice/GeneModelConsolidation/intermediateFiles/uniprot-all.fasta -max_target_seqs 1 -outfmt 6 -evalue 1e-5 -num_threads 2 >$i.outfmt6\n";}' > blastp.sh

adispatch blastp.sh

rm stringtie.exons.fa.transdecoder_dir/longest_orfs.pep.blastp

cat longest_orfs.pep.split/orf_*outfmt6 >> stringtie.exons.fa.transdecoder_dir/longest_orfs.pep.blastp
</code></pre>

**Solution 2:**

You can submit fasta sequences of all queiries to blastp.

In terminal, type the following command,
<pre><code>perl -e 'print "#!/bin/bash\n#SBATCH --partition normal\n#SBATCH --mem 32G\n#SBATCH -c 12\n#SBATCH -t 240:00:00\n\n\nsource /com/extra/BLAST/LATEST/load.sh;blastp -query /home/jjduan/06ricesplicing/ricealtsplice/GeneModelConsolidation/intermediateFiles/stringtie.exons.fa.transdecoder_dir/longest_orfs.pep -db /home/jjduan/06ricesplicing/ricealtsplice/GeneModelConsolidation/intermediateFiles/uniprot-all.fasta -max_target_seqs 1 -outfmt 6 -evalue 1e-5 -num_threads 12 > /home/jjduan/06ricesplicing/ricealtsplice/GeneModelConsolidation/intermediateFiles/stringtie.exons.fa.transdecoder_dir/longest_orfs.pep.outfmt6";' > allblastp.sh

qx -n 10 -c 8 -t 100:00:00 --no-scratch "source /com/extra/BLAST/LATEST/load.sh;blastp -query /cealtsplice/GeneModelConsolidation/intermediateFiles/stringtie.exons.fa.transdecoder_dir/longest_orfs.pep -db /home/jjduan/06ricesplicing/ricealtsplice/GeneModelConsolidation/intermediateFiles/uniprot-all.fasta -max_target_seqs 1 -outfmt 6 -evalue 1e-5 -num_threads 12 > /home/jjduan/06ricesplicing/ricealtsplice/GeneModelConsolidation/intermediateFiles/stringtie.exons.fa.transdecoder_dir/longest_orfs.pep.outfmt6"
</code></pre>
