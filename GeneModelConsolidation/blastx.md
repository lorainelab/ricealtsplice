## Database:

The pre-formatted nr database was downloaded by wget command from ncbi ftp server on 7th June 2016.

<pre><code>cd intermediateFiles
mkdir nr-db
cd nr-db
wget -r -l1 --no-parent -A nr*gz* ftp://ftp.ncbi.nlm.nih.gov/blast/db/
cd ftp.ncbi.nlm.nih.gov/blast/db/
ls nr*gz|xargs -i echo tar -zxvf {}|sh
</code></pre>


## Tool:

BLAST 2.2.26+  with paramters -max_target_seqs 1 -outfmt 6 -evalue 1e-5

## Instruction:

Firstly you split fasta sequences of queries into 300 files. Files will only be broken at fa record boundaries. Then you generate the bash file for submitting jobs parallelly.

In terminal, type following command,

<pre><code>cd intermediateFiles
rm -rf all.cds
mkdir -p all.cds
cd all.cds
../../src/faSplit sequence ../MSU7_IRGSP_STRG.cds 300 cds_
cd ..

perl -e '@a=glob("/home/jjduan/06ricesplicing/ricealtsplice/GeneModelConsolidation/intermediateFiles/all.cds/cds_*");print "#!/bin/bash\n#SBATCH --partition normal\n#SBATCH --mem-per-cpu 16G\n#SBATCH -c 2\n#SBATCH -t 240:00:00\n\n\n";foreach $i (@a){print "source /com/extra/BLAST/LATEST/load.sh;blastx -query $i -db /home/jjduan/06ricesplicing/ricealtsplice/GeneModelConsolidation/intermediateFiles/nr-db/ftp.ncbi.nlm.nih.gov/blast/db/nr -max_target_seqs 1 -outfmt 6 -evalue 1e-5 -num_threads 2 >$i.outfmt6\n";}' > blastx.sh

adispatch blastx.sh

rm MSU7_IRGSP_STRG.cds.blastx

cat all.cds/cds_*outfmt6 >>MSU7_IRGSP_STRG.cds.blastx
</code></pre>
