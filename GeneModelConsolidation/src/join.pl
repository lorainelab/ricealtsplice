#!/usr/local/bin/perl
# Wed Mar 23 16:02:53 EDT 2016
# Author: Jinjie Duan [jjduan@birc.au.dk]

use strict;
use warnings;
use Data::Dumper;

if (@ARGV != 2){
	die "USAGE: $0 stringtie.exons.fa.transdecoder.bed longest_orfs.pep.blastp > output\nThe script is used to join two files together by the same ORF ID.\n";
}

my %blast;
my $nohit;
my @nohit;

open IN2, "$ARGV[1]";
while(<IN2>){
	chomp;
	my @line = split /\t/,$_;
	$blast{$line[0]} = $_;
	@nohit = ("NA") x @line;
	$nohit = join "\t",@nohit;	
}
close IN2;


open IN1, "$ARGV[0]";
my $track = <IN1>;
while(<IN1>){
	chomp;
	my @line = split /\t/, $_;
	my ($id,$type,$len) = $line[3] =~ /ID=([^;]*);.*_type:(.*)_len:(\d+)/;
	if (exists $blast{$id}){
		print "$_\t$id\t$type\t$len\t$blast{$id}\n";
	}else{
		print "$_\t$id\t$type\t$len\t$nohit\n";
	}
}
close IN1;
