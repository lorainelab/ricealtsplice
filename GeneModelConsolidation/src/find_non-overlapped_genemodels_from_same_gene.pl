#!/usr/local/bin/perl
# Mon Apr 25 23:28:37 EDT 2016
# Author: Jinjie Duan [jjduan@birc.au.dk]

use strict;
use warnings;
use Data::Dumper;
use List::Util qw( min max );

if (@ARGV != 1){
	die "USAGE: $0 MSU7_IRGSP_STRG.bed > output\nThe script is used to find how often and for which genes in MSU7 or RAPDB annotated gene models that have the same gene id but do not overlap?\n";
}

my %h;

open IN0, "$ARGV[0]";
while(<IN0>){
	chomp;
	
	my @line = split /\t/,$_;
	
	my $geneid;
	
	if ($line[3] =~ /^(STRG\.\d*)/){

		$geneid = $1;	

	}elsif($line[3] =~ /^(Os.*)\-/){
		
		$geneid = $1;
		
	}elsif($line[3] =~ /^(LOC.*)\.\d/){
		
		$geneid = $1;
		
	}	
	
	# ignore gene models with ChrUn.fgenesh.mRNA.
	if ($line[3] !~ /^Chr/){
		# key of hash is gene ID and transcript ID. In array, first element is the start of gene model, second element is the end. 
		@{$h{$geneid}{$line[3]}{'cor'}} = ($line[1],$line[2]);
		$h{$geneid}{$line[3]}{'annotation'} = $line[13];
	}
	
}
close IN0;
#print Dumper \%h;

foreach my $geneid (sort keys %h){

	my @trid = keys %{$h{$geneid}};
	
	# find non overlapped gene models from the same gene ID.
	for (my $i = 0; $i < @trid; $i++){
		
		for (my $j = $i+1; $j < @trid; $j++){
			
			my $noOver;
			
			$noOver = noOverlap($h{$geneid}{$trid[$i]}{'cor'}[0],$h{$geneid}{$trid[$i]}{'cor'}[1],$h{$geneid}{$trid[$j]}{'cor'}[0],$h{$geneid}{$trid[$j]}{'cor'}[1]);
			
			if ($noOver == 1){
			
				print "$geneid\t$trid[$i]\t$trid[$j]\t$h{$geneid}{$trid[$i]}{'annotation'}\t$h{$geneid}{$trid[$j]}{'annotation'}\n";
			
			}
		}
	}
}

sub noOverlap{
    
    my ($start1, $end1, $start2, $end2) = @_;
    my $index;
    
    if (($start1 <= $start2 && $start2 <= $end1) || ($start2 <= $start1 && $start1 <= $end2)){
        $index = 0;
    }else{
        $index = 1;
    }
    
    # if two regions do not overlap, return 1; return 0 for overlaping.
    return ($index);
}    