#!/usr/local/bin/perl
# Mon Feb 29 09:55:23 EST 2016
# Author: Jinjie Duan

use strict;
use warnings;

if (@ARGV != 4){
	die "USAGE: $0 *.bed.merged *.bed ID.map GeneModelGroups.txt > output.file\n The first input bed file is the output file from bedtools merge. The second bed file is the orignial bed file (i.e. the input file of bedtools merge). The third file is the output of intermidiate result for storing the map of N.M tag and original ID.\nThe script assigns the group ID for merged gene models.\n";
}

my ($N,$M) = (1,1);
my %hash;

open OUT, ">$ARGV[2]";
open OUT2, ">$ARGV[3]";

open IN, "$ARGV[0]";
while(<IN>){
	chomp;
	my @line = split /\t/,$_;
	my @id = split /,/,$line[4];
	
	foreach my $i (@id){
		my $gene;	
		my $source;
		if ($i =~ m/^LOC/){
			$source = "MSU7";
			$gene = $i;
			$gene =~ s/\..*//;
			
		}elsif ($i =~ m/^Chr/){
			$source = "MSU7";
			$gene = $i;
			
		}elsif ($i =~ m/STRG/){
			$source = "STRG";
			$i =~ m/^(STRG.\d*)/;
			$gene = $1;
			
		}else{
			$source = "RAPDB";
			$gene = $i;
			$gene =~ s/-.*//;
			
		}
		$hash{$i}{"tag"} = "Osj.$N.$M";
		$hash{$i}{"source"} = $source;
		$hash{$i}{"gene"} = $gene;
		
		print OUT "$N\t$M\tOsj.$N.$M\t$i\t$gene\t$source\n";
		print OUT2 "$line[0]\t$line[1]\t$line[2]\t$line[3]\t$i\t$N\t$M\t$source\n";
		
		$M += 1;
		
	}
	$N += 1;
	$M = 1;
	
}
close IN;
close OUT;
close OUT2;

open IN2,"$ARGV[1]";
while(<IN2>){
	chomp;
	my @line = split /\t/,$_;
	
	# update the gene ID
	$line[12] = $hash{$line[3]}{"gene"};
	
	# update to the new N.M format id
	$line[3] = $hash{$line[3]}{"tag"};
	
	my $new = join "\t",@line;
	print "$new\n";
}
close IN2;
