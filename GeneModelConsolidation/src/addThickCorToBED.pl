#!/usr/local/bin/perl
# Wed Apr  6 07:55:06 EDT 2016
# Author: Jinjie Duan
# Email : jjduan@birc.au.dk

use strict;
use warnings;

if (@ARGV != 2){
	die "USAGE: $0 stringtie.bed stringtie.longestORFonly.bed > stringtie.corrected.bed
This script modifies thickStart (col. 7) , thickEnd(col.8) of gene models which have ORF in original stringtie output.
The first input bed file is the original output of stringTie. The second input file contains only stringtie gene models which have the predicted longest ORF.
";
}

my %orf;
my ($id,$note,$locus);

open IN2, "$ARGV[1]";
while(<IN2>){
	chomp;
	my @line = split /\t/,$_;
	my @id = split /\|/,$line[3];
	$orf{$id[0]}{"thickStart"} = $line[6];
	$orf{$id[0]}{"thickEnd"} = $line[7];
}
close IN2;

open IN1, "$ARGV[0]";
while(<IN1>){
	chomp;
	my @line = split /\t/,$_;
	if (exists $orf{$line[3]}){
		$line[6] = $orf{$line[3]}{"thickStart"};
		$line[7] = $orf{$line[3]}{"thickEnd"};
		my $new_line = join "\t",@line;
		print "$new_line\n";
	}else{
		print "$_\n";	
	}
}
close IN1;