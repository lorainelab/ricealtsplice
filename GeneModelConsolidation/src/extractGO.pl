#!/usr/local/bin/perl
# Thu Mar  3 15:44:06 EST 2016
# Author: Jinjie Duan
# Email: jjduan.birc@gmail.com

use strict;
use warnings;

if (@ARGV != 1){
	die "USAGE: $0 *.gff\nThe script prints the GO term row by row.\n";
}

my %mRNA;
my ($id,$note,$locus);

open IN1, "$ARGV[0]";
while(<IN1>){
	chomp;
	my @line = split /\t/, $_;
	if ($line[2] eq "mRNA"){
		$line[8] =~ m/ID=([^;]*);/;
		$id = $1;
		#print "$line[8]\t$id\n";
		$line[8] =~ m/Locus_id=([^;]*)/;
		$locus = $1;
		
		my @GO = ($line[8] =~ m/(GO:\d+)/g);
		
		foreach my $i (@GO){
			print "$id\t$i\n";	
		}	
	}
	
}
close IN1;