#!/usr/local/bin/perl
# Fri Feb 26 04:29:35 EST 2016
# Author: Jinjie Duan

use strict;
use warnings;

if (@ARGV != 2){
	die "USAGE: $0 *.gff3 *.bed >*.corrected.bed\n The input bed file is the output file from GFF Toolkit v0.1 (https://galaxy.cbio.mskcc.org/). The script corrects the start(col.2), thickStart (col. 7) , thickEnd(col.8) and add two extra fields (gene name and note) in.\n";
}

my %mRNA;
my ($id,$note,$locus);

open IN1, "$ARGV[0]";
while(<IN1>){
	chomp;
	if ($_ =~ /\w/){
		my @line = split /\t/, $_;
		if ($line[2] eq "mRNA"){
			$line[8] =~ m/ID=([^;]*);/;
			$id = $1;
			#print "$line[8]\t$id\n";
			$line[8] =~ m/Locus_id=([^;]*)/;
			$locus = $1;
			
			if ($line[8] =~ m/Note=([^;]*)/){
				$note = $1;
			}else{
				$note = "Predicted gene.";	
			}
			
			$mRNA{$id}{"locus"} = $locus;
			$mRNA{$id}{"note"} = $note;
			
		}elsif($line[2] eq "CDS"){
			$line[8] =~ m/Parent=(.*)/;
			my $parent = $1;
			if (exists $mRNA{$parent}{"cdsStart"}){
				
				# if occur smaller start or bigger end coordinate of cds, update to hash
				if ($mRNA{$parent}{"cdsStart"} > $line[3]){
					$mRNA{$parent}{"cdsStart"} = $line[3];
				}
				if ($mRNA{$parent}{"cdsEnd"} < $line[4]){
					$mRNA{$parent}{"cdsEnd"} = $line[4];
				}
			}else{
				$mRNA{$parent}{"cdsStart"} = $line[3];
				$mRNA{$parent}{"cdsEnd"} = $line[4];	
			}
			
		}
		
	}
}
close IN1;

open IN2, "$ARGV[1]";
while(<IN2>){
	chomp;
	my @line = split /\t/,$_;
	$line[0] =~ s/chr0/chr/;
	$line[0] =~ s/chr/Chr/;
	$line[4] = "0";
	
	if ($mRNA{$line[3]}{"note"} =~ /^Non-protein/){
		# if it is annotated with Non-protein coding, the thickEnd and thickStart assign to the same coordinate of chrStart 	
		$line[6] = $line[1];
		$line[7] = $line[1];
		
	}elsif(exists $mRNA{$line[3]}{"cdsEnd"}){
		# if the transcirpt has cds, update thickStart and thickEnd
		# the corrodinate is based on 0-start system in bed, but it's 1-start in gff3. so we need to substract 1 for start coordinate.
			$line[6] = $mRNA{$line[3]}{"cdsStart"} -1;
			$line[7] = $mRNA{$line[3]}{"cdsEnd"};	
	}
	
	push (@line, $mRNA{$line[3]}{"locus"});
	push (@line, $mRNA{$line[3]}{"note"});

	my $newLine = join "\t", @line;
	print "$newLine\n";
}
close IN2;