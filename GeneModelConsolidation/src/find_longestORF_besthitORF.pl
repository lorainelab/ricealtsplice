#!/usr/local/bin/perl
# Wed Mar 23 16:02:53 EDT 2016
# Author: Jinjie Duan [jjduan@birc.au.dk]

## add one input some transcripts doesn't have ORF need to be added in the final output. remeber to work on it on 29 Mar.

use strict;
use warnings;
use Data::Dumper;

if (@ARGV != 1){
	die "USAGE: $0 join.output \n";
}

open IN1, "$ARGV[0]";
my $track = <IN1>;
my %hash;

while(<IN1>){
	chomp;
	my @line = split /\t/, $_;
	my $trid = $line[0];
	my $orf = $line[12];
	my $type = $line[13];
	my $len = $line[14];
	my $hit = $line[16];
	my $score = $line[26];
	
	if (exists $hash{$trid}{'longest'}){
		if ($hash{$trid}{'longest'}{'len'} < $len ){
			$hash{$trid}{'longest'}{'orf'} = $orf ;
			$hash{$trid}{'longest'}{'len'} = $len ;
			$hash{$trid}{'longest'}{'type'} = $type ;
			$hash{$trid}{'longest'}{'score'} = $score ;
			$hash{$trid}{'longest'}{'hit'} = $hit ;
		}		
	}else{
		$hash{$trid}{'longest'}{'orf'} = $orf ;
		$hash{$trid}{'longest'}{'len'} = $len ;
		$hash{$trid}{'longest'}{'type'} = $type ;
		$hash{$trid}{'longest'}{'score'} = $score ;
		$hash{$trid}{'longest'}{'hit'} = $hit ;
	}
	
	if (exists $hash{$trid}{'besthit'}){
		if ($score ne "NA" && $hash{$trid}{'besthit'}{'score'} < $score){
			$hash{$trid}{'besthit'}{'orf'} = $orf ;
			$hash{$trid}{'besthit'}{'len'} = $len ;
			$hash{$trid}{'besthit'}{'score'} = $score ;
			$hash{$trid}{'besthit'}{'type'} = $type ;
			$hash{$trid}{'besthit'}{'hit'} = $hit ;
		}		
	}elsif($score ne "NA"){
		$hash{$trid}{'besthit'}{'orf'} = $orf ;
		$hash{$trid}{'besthit'}{'len'} = $len ;
		$hash{$trid}{'besthit'}{'score'} = $score ;
		$hash{$trid}{'besthit'}{'type'} = $type ;
		$hash{$trid}{'besthit'}{'hit'} = $hit ;
	}else{
		$hash{$trid}{'besthit'}{'orf'} = "NA" ;
		$hash{$trid}{'besthit'}{'len'} = "NA" ;
		$hash{$trid}{'besthit'}{'score'} = -1 ;
		$hash{$trid}{'besthit'}{'type'} = "NA" ;
		$hash{$trid}{'besthit'}{'hit'} = "NA" ;
	}
		
}
close IN1;

print "#TranscriptID\tLongestORFID\tORFlen\tORFtype\tORFhit\tORFscore\tMaxScoreORFID\tORFlen\tORFtype\tORFhit\tORFscore\n";
foreach my $ID (sort keys %hash){
	print "$ID\t$hash{$ID}{'longest'}{'orf'}\t$hash{$ID}{'longest'}{'len'}\t$hash{$ID}{'longest'}{'type'}\t$hash{$ID}{'longest'}{'hit'}\t$hash{$ID}{'longest'}{'score'}\t$hash{$ID}{'besthit'}{'orf'}\t$hash{$ID}{'besthit'}{'len'}\t$hash{$ID}{'besthit'}{'type'}\t$hash{$ID}{'besthit'}{'hit'}\t$hash{$ID}{'besthit'}{'score'}\n";
}

#print Dumper (\%hash);
