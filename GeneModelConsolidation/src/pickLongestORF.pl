#!/usr/local/bin/perl
# Wed Mar 23 16:02:53 EDT 2016
# Author: Jinjie Duan [jjduan@birc.au.dk]

## add one input some transcripts doesn't have ORF need to be added in the final output. remeber to work on it on 29 Mar.

use strict;
use warnings;
use Data::Dumper;

if (@ARGV != 2){
	die "USAGE: $0 stringtie.exons.fa.transdecoder.bed stringtie.exons.fa.transdecoder.genome.corrected.bed > output\n";
}

my %mRNA;
my ($id,$note,$locus);

open IN1, "$ARGV[0]";
my $track = <IN1>;
my %hash;

while(<IN1>){
	chomp;
	my @line = split /\t/, $_;
	my $info = $line[3];
	my ($id,$len) = $info =~ /ID=([\w\|\.]+);.*_len:(\d+)/;
	
	if (exists $hash{$line[0]}){
		if ($hash{$line[0]}{'len'} < $len ){
			$hash{$line[0]}{'orf'} = $id ;
			$hash{$line[0]}{'len'} = $len ;	
		}		
	}else{
		$hash{$line[0]}{'orf'} = $id ;
		$hash{$line[0]}{'len'} = $len ;
	}
}
close IN1;

#print Dumper (\%hash);

open IN2, "$ARGV[1]";
while(<IN2>){
	chomp;
	my @line = split /\t/,$_;
	my ($tr) = $line[3] =~ /(STRG.*)\|/; 
#	print $tr."\n";
	if ($hash{$tr}{'orf'} eq $line[3]){
		print "$_\n";
	}
}
close IN2;