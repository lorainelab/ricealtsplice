#!/usr/local/bin/perl
# Thu Mar  3 15:44:06 EST 2016
# Author: Jinjie Duan
# Email: jjduan.birc@gmail.com

use strict;
use warnings;

if (@ARGV != 2){
	die "USAGE: $0 ID.map MSU7_IRGSP.GO > output.file\nThe script links the GO terms to N.M tag\n";
}

my %hash;

open IN2, "$ARGV[1]";
while(<IN2>){
	chomp;
	my @line = split /\t/, $_;
	if (! exists ($hash{$line[0]})){
		$hash{$line[0]}[0] = $line[1];	
	}else{
		push (@{$hash{$line[0]}}, $line[1]);
	}
}
close IN2;


open IN1, "$ARGV[0]";
while(<IN1>){
	chomp;
	my @line = split /\t/, $_;
	if (exists $hash{$line[3]}){
		foreach my $i (@{$hash{$line[3]}}){
			print "$_\t$i\n";
		}
	}else{
		print "$_\tNA\n";
	}
}
close IN1;

