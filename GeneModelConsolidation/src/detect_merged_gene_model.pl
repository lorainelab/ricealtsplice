#!/usr/local/bin/perl
# Fri Apr 22 13:55:53 EDT 2016
# Author: Jinjie Duan [jjduan@birc.au.dk]

use strict;
use warnings;
use Data::Dumper;

if (@ARGV != 2){
	die "USAGE: $0 MSU7_IRGSP_STRG.bed MSU7_IRGSP_STRG.bed.merged  > merged_gene_model.id\nThe script is used to detect potiential misassembled gene models which might be created by merging multiple known gene models\nIt will creat a output file named as $ARGV[0].non-merged.bed for storing gene models after eliminating those misassembled gene models\n";
}

my %h;

open IN0, "$ARGV[0]";
while(<IN0>){
	chomp;
	my @line = split /\t/,$_;
	my $db = substr($line[3],0,2);
	@{$h{$line[3]}} = ($line[1],$line[2],$db,$line[3]);
}
close IN0;
#print Dumper \%h;

# store the id of merged gene models
my %merged;

open IN1, "$ARGV[1]";
while(<IN1>){
	chomp;
	my @line = split /\t/, $_;
	my @gene = split /,/, $line[4];
	
	my $sum;
	for (my $i = 0; $i < @gene; $i++){
		
		$sum = 0;
		my @overlapgene;
		
		# find genes overlapped with query gene model
		for (my $j = 0; $j < @gene; $j++){
			my $overlap = overlap($h{$gene[$i]}[0],$h{$gene[$i]}[1],$h{$gene[$j]}[0],$h{$gene[$j]}[1]);			
			if ($overlap ==1){
				push @overlapgene,$gene[$j];
			}
		}
		
		
		# check if there has 2 genes overlapped with the query gene model do not overlap each other
		OUT: for (my $m = 0; $m < @overlapgene; $m ++){
			for (my $n = 0; $n < @overlapgene; $n++){
				my $overlap = overlap($h{$overlapgene[$m]}[0],$h{$overlapgene[$m]}[1],$h{$overlapgene[$n]}[0],$h{$overlapgene[$n]}[1]);			
				if (($gene[$i] =~ /^ST/) && ($h{$overlapgene[$m]}[2] eq $h{$overlapgene[$n]}[2]) && ($h{$overlapgene[$m]}[3] ne $h{$overlapgene[$n]}[3]) && $overlap == 0 ){
					print "$gene[$i]\n";
					$merged{$gene[$i]} = 1;
					last OUT;
				}
			}
		}	
		
	}
}
close IN1;

# print out non-merged gene models
open OUT, ">$ARGV[0].non-merged.bed";
open IN0, "$ARGV[0]";
while(<IN0>){
	chomp;
	my @line = split /\t/,$_;
	if (!exists $merged{$line[3]}){
		print OUT "$_\n";
	}
}
close IN0;
close OUT;

sub overlap{
    
    my ($start1, $end1, $start2, $end2) = @_;
    my $index;
    
    if (($start1 <= $start2 && $start2 <= $end1) || ($start2 <= $start1 && $start1 <= $end2)){
        $index = 1;
    }else{
        $index = 0;
    }
    
    # if two regions have overlap, return 1; return 0 for non overlaping.
    return ($index);
}    