#!/bin/bash

# Dependencies:
# bedtools version 2.25.0
# bgzip, tabix - from https://github.com/samtools/htslib 
# BLAST 2.2.26+

clear

echo "The pipeline starts now."

set -v

mkdir -p intermediateFiles

#######################################################################################################################################
#
# uncompress files in data folder and move to intermediateFiles folder
#
#######################################################################################################################################
gunzip -c data/IRGSP-1.0_representative_2015-03-31/transcripts_exon.gff.gz > intermediateFiles/re_transcripts_exon.gff

gunzip -c data/IRGSP-1.0_representative_2015-03-31/transcripts.gff.gz > intermediateFiles/re_transcripts.gff

gunzip -c data/IRGSP-1.0_predicted_2015-03-31/transcripts_exon.gff.gz > intermediateFiles/pre_transcripts_exon.gff

gunzip -c data/IRGSP-1.0_predicted_2015-03-31/transcripts.gff.gz > intermediateFiles/pre_transcripts.gff

gunzip -c data/stringtie.bed.gz > intermediateFiles/stringtie.bed

gunzip -c data/stringtie.gtf.gz >intermediateFiles/stringtie.gtf

gunzip -c data/O_sativa_japonica_Oct_2011.fa.gz >intermediateFiles/O_sativa_japonica_Oct_2011.fa

gunzip -c data/O_sativa_japonica_Oct_2011.GO.gz >intermediateFiles/O_sativa_japonica_Oct_2011.GO

gunzip -c data/O_sativa_japonica_Oct_2011.bed.gz >intermediateFiles/O_sativa_japonica_Oct_2011.bed

gunzip -c data/uniprot-all.fasta.gz >intermediateFiles/uniprot-all.fasta

gunzip -c data/O_sativa_japonica_Oct_2011.cds.gz >intermediateFiles/O_sativa_japonica_Oct_2011.cds

gunzip -c data/IRGSP-1.0_representative_2015-03-31/IRGSP-1.0_cds_2016-03-09.fasta.gz >intermediateFiles/re_transcripts.cds.fa

gunzip -c data/IRGSP-1.0_predicted_2015-03-31/IRGSP-1.0_predicted-cds_2016-03-09.fasta.gz >intermediateFiles/pre_transcripts.cds.fa

#######################################################################################################################################
#
# convert gff/gff3 to bed format
#
#######################################################################################################################################

# convert gff to bed for representative gene set of RAP-DB database
python src/gff3_to_bed_converter.py -q intermediateFiles/re_transcripts_exon.gff -o intermediateFiles/re_transcripts.bed

# substracts 1 from start position
# modifies thickStart and thickEnd
# adds fields 13 and 14 for BED-detail
perl src/correctBED.pl intermediateFiles/re_transcripts.gff intermediateFiles/re_transcripts.bed | sort -k1,1 -k2,2n >intermediateFiles/re_transcripts.corrected.bed

# convert gff to bed for predicted gene set of RAP-DB database
python src/gff3_to_bed_converter.py -q intermediateFiles/pre_transcripts_exon.gff -o intermediateFiles/pre_transcripts.bed

perl src/correctBED.pl intermediateFiles/pre_transcripts.gff intermediateFiles/pre_transcripts.bed |sort -k1,1 -k2,2n >intermediateFiles/pre_transcripts.corrected.bed

#######################################################################################################################################
#
# predict ORF for stringtie models
#
#######################################################################################################################################
# remove the first two rows in stringtie.gtf
tail -n+3 intermediateFiles/stringtie.gtf > intermediateFiles/stringtie.gtf.withoutTitle

# extarct cDNA fasta sequences
perl src/TransDecoder-2.0.1/util/cufflinks_gtf_genome_to_cdna_fasta.pl intermediateFiles/stringtie.gtf.withoutTitle intermediateFiles/O_sativa_japonica_Oct_2011.fa > intermediateFiles/stringtie.exons.fa

# sort fasta sequences by header
awk 'BEGIN{RS=">"} NR>1 {gsub("\n", "\t"); print ">"$0}' intermediateFiles/stringtie.exons.fa | sort -t ' ' -k1,1 | awk '{sub("\t", "\n"); gsub("\t", ""); print $0}' > intermediateFiles/stringtie.exons.fa.sorted

# replace the fasta name
mv intermediateFiles/stringtie.exons.fa.sorted intermediateFiles/stringtie.exons.fa 

cd intermediateFiles

../src/TransDecoder-2.0.1/TransDecoder.LongOrfs -t stringtie.exons.fa -S

# build DB for blastp
makeblastdb -in uniprot-all.fasta -dbtype prot

# blastp all ORFs for homology to Swissprot protein database
# database: fasta of Reviewed (550,740) Swiss-Prot downloaded from [http://www.uniprot.org/downloads](http://www.uniprot.org/downloads)  with release-3/17/16
# tool: BLAST 2.2.26+  with paramters -max_target_seqs 1 -outfmt 6 -evalue 1e-5
# you can either run blastp locally with following command, or run it parallelly in cluster. The instruction of running blastp on UNCC cluster can be found in blastp.md. 
blastp -query stringtie.exons.fa.transdecoder_dir/longest_orfs.pep -db uniprot-all.fasta -max_target_seqs 1 -outfmt 6 -evalue 1e-5 -num_threads 1 > stringtie.exons.fa.transdecoder_dir/longest_orfs.pep.blastp

../src/TransDecoder-2.0.1/TransDecoder.Predict -t stringtie.exons.fa --retain_blastp_hits stringtie.exons.fa.transdecoder_dir/longest_orfs.pep.blastp

cd ..

# convert the transcript structure GTF file to an alignment-GFF3 formatted file
perl ./src/TransDecoder-2.0.1/util/cufflinks_gtf_to_alignment_gff3.pl intermediateFiles/stringtie.gtf.withoutTitle >intermediateFiles/stringtie.gff3

# generate a genome-based coding region annotation file
perl ./src/TransDecoder-2.0.1/util/cdna_alignment_orf_to_genome_orf.pl intermediateFiles/stringtie.exons.fa.transdecoder.gff3 intermediateFiles/stringtie.gff3 intermediateFiles/stringtie.exons.fa > intermediateFiles/stringtie.exons.fa.transdecoder.genome.gff3

# convert gff3 to BED format
python src/gff3_to_bed_converter.py -q intermediateFiles/stringtie.exons.fa.transdecoder.genome.gff3 -o intermediateFiles/stringtie.exons.fa.transdecoder.genome.bed

cd intermediateFiles 

# correct the thickStart and thickEnd coordinates for stringtie based on CDS range
perl ../src/correctBED.pl stringtie.exons.fa.transdecoder.genome.gff3 stringtie.exons.fa.transdecoder.genome.bed |sort -k1,1 -k2,2n >stringtie.exons.fa.transdecoder.genome.corrected.bed 

# Because transDecoder predictes multiple ORFs for one gene model, we pick the longest ORF to represent the corresponding gene model
perl ../src/pickLongestORF.pl stringtie.exons.fa.transdecoder.bed stringtie.exons.fa.transdecoder.genome.corrected.bed > stringtie.longestORFonly.bed

# the ID map between orignial stringtie ID and the corresponding ORF ID.
cut -f 4 stringtie.longestORFonly.bed | tr -s '|' '\t' >stringtie.ORF.IDmap

# For the original output of stringtie, we assign thickStart and thickEnd of gene models which have longest ORF
perl ../src/addThickCorToBED.pl stringtie.bed stringtie.longestORFonly.bed > stringtie.corrected.bed

# we join two files together by the same ORF ID. One file is the all predicted ORF bed file. The other is blastp result of all predicted ORF.
perl ../src/join.pl stringtie.exons.fa.transdecoder.bed stringtie.exons.fa.transdecoder_dir/longest_orfs.pep.blastp > joined.table

# One transcript can have multiple ORFs. So for each transcript, we report the longest ORF and the ORF with higest blastp score. 
perl ../src/find_longestORF_besthitORF.pl joined.table > joined.table.assigned.output

cd ..

# look at Statistic of stringtie gene model.Rmd to get a summary statistics.

#######################################################################################################################################
#
# Gene model consolidation
#
#######################################################################################################################################
# combine all 4 BED files into one BED file and remove the duplicated gene models
sort -u -k1,1 -k2,3n -k6,8 -k10,12 intermediateFiles/re_transcripts.corrected.bed intermediateFiles/pre_transcripts.corrected.bed intermediateFiles/O_sativa_japonica_Oct_2011.bed intermediateFiles/stringtie.corrected.bed > intermediateFiles/MSU7_IRGSP_STRG.bed

# merge overlapped region
bedtools merge -i intermediateFiles/MSU7_IRGSP_STRG.bed -s -c 4,6 -o collapse > intermediateFiles/MSU7_IRGSP_STRG.bed.merged
 
# replace the transcript ID by tag N.M to original bed file, and also record the orinigal ID
perl src/assignGroup.pl intermediateFiles/MSU7_IRGSP_STRG.bed.merged intermediateFiles/MSU7_IRGSP_STRG.bed intermediateFiles/ID.map intermediateFiles/GeneModelGroups.txt >intermediateFiles/MSU7_IRGSP_STRG.NM.bed

# prepare GO annotation into one file for MSU7 and RAPDB gene models
perl src/extractGO.pl intermediateFiles/re_transcripts.gff >intermediateFiles/re_transcripts.GO

cat intermediateFiles/re_transcripts.GO intermediateFiles/O_sativa_japonica_Oct_2011.GO >>intermediateFiles/MSU7_IRGSP.GO

# link GO term to N.M identifier
perl src/linkGO.pl intermediateFiles/ID.map intermediateFiles/MSU7_IRGSP.GO > intermediateFiles/MSU7_IRGSP_STRG.GO

#######################################################################################################################################
#
# Remove potential misassembled gene models which might be created by mergeing multiple known gene models into one gene model.
# & creat a new set of gene models & assign N.M tag for it.
#
#  we define misassembled gene model as in one cluster, it has overlap with other gene models which are
# 1. originally from the same public database either RAP or MSU7 
# 2. transcripted from different gene
# 3. non-overlapped among each other

#######################################################################################################################################
# the output is id of potential misassembled gene models.It also creats a file named as MSU7_IRGSP_STRG.bed.non-merged.bed to store non-merged gene models
perl src/detect_merged_gene_model.pl intermediateFiles/MSU7_IRGSP_STRG.bed intermediateFiles/MSU7_IRGSP_STRG.bed.merged > intermediateFiles/merged_gene_model.id

# merge overlapped region
bedtools merge -i intermediateFiles/MSU7_IRGSP_STRG.bed.non-merged.bed -s -c 4,6 -o collapse > intermediateFiles/MSU7_IRGSP_STRG.bed.non-merged.bed.merged
 
# replace the transcript ID by tag N.M to original bed file, and also record the orinigal ID
perl src/assignGroup.pl intermediateFiles/MSU7_IRGSP_STRG.bed.non-merged.bed.merged intermediateFiles/MSU7_IRGSP_STRG.bed.non-merged.bed intermediateFiles/non-merged.ID.map intermediateFiles/non-merged.GeneModelGroups.txt >intermediateFiles/MSU7_IRGSP_STRG.bed.non-merged.NM.bed

# link GO term to N.M identifier
perl src/linkGO.pl intermediateFiles/non-merged.ID.map intermediateFiles/MSU7_IRGSP.GO > intermediateFiles/MSU7_IRGSP_STRG.non-merged.GO

#######################################################################################################################################
#
# check how often and for which genes in MSU7 or RAPDB annotated gene models that have the same gene id but do not overlap
#
#######################################################################################################################################
# count how many gene have non-overlapped gene models in each database. note: ST represents stringtie database; Os represents RAPDB database; LO represent MSU7 database.
perl src/find_non-overlapped_genemodels_from_same_gene.pl intermediateFiles/MSU7_IRGSP_STRG.bed.non-merged.bed > intermediateFiles/non-overlapped-gene-models-from-same-gene.txt

cut -f 1 intermediateFiles/non-overlapped-gene-models-from-same-gene.txt|sort|uniq|cut -c 1,2|sort|uniq -c

#######################################################################################################################################
#
# compress several files and move them to the folder result
#
###################################################################################################################################
gzip -c intermediateFiles/non-merged.GeneModelGroups.txt > result/GeneModelGroups.txt.gz

sort -k1,1 -k2,2n intermediateFiles/MSU7_IRGSP_STRG.bed.non-merged.NM.bed | bgzip > result/non-redundant.gene.models.bed.gz

tabix -s 1 -b 2 -e 3 result/non-redundant.gene.models.bed.gz

gzip -c intermediateFiles/MSU7_IRGSP_STRG.non-merged.GO > result/non-redundant.gene.models.GO.gz

sort -k1,1 -k2,2n intermediateFiles/stringtie.exons.fa.transdecoder.genome.corrected.bed |bgzip > result/stringtie.transdecoder.allORF.bed.gz

tabix -s 1 -b 2 -e 3 result/stringtie.transdecoder.allORF.bed.gz
#######################################################################################################################################
set +v

echo "The pipeline is done. You get the non-redundant gene models of rice now. You can find the final annotation file under result folder."

echo "Congratulations!!! "