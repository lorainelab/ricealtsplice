###Gene model consolidation

To assess alternative splicing, we created a single, non-redundant set of gene models based on the following 3 set of gene models downloaded from public rice database and 1 set of predicted gene model from stringtie (gene prediction software).

* IRGSP-1.0_representative_2015-03-31 (Downloaded from http://rapdb.dna.affrc.go.jp/)
* IRGSP-1.0_predicted_2015-03-31 (Downloaded from http://rapdb.dna.affrc.go.jp/)
* MSU7 models (Downloaded from IGB quickload ftp: O_sativa_japonica_Oct_2011.bed.gz)
* Output from stringtie

**Firstly**, we formatted 4 dataset into BED detail format, and consolidated them into one BED detail file and sorted it. 

**Secondly**, we clustered gene models into locus groups.

The primary deliverable: **result/MSU7_IRGSP_STRG.NM.bed**

A BED file containing all the gene models in which field four (id field) is replaced with a unique identifier that looks like this:

N.M

where N are integers > 0. All gene models in a cluster share the same N value.

To be assigned to the same N number, gene models should come from the same gene. For purposes of this study, we define that as: gene models produced from overlapping regions.

###Predict ORF for gene models from stringtie
We used [TransDecoder-2.0.1](http://transdecoder.github.io/#incl_homology) with default parameters to predict ORF.