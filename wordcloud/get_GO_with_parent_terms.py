#python 3, needs conversion to python 2

"""
This script will make a list of LOC terms, from a file wich is given as user input, and cross-index the gene_association.gramene_oryza returning the GO-terms in a list for the word-cloud website, http://www.wordclouds.com/.
Note, spaces must be replaced with tildas for www.wordclouds.com to recognize a group of words as a phrase
"""



def Query():
	LOCs = []
	GO = []
	with open("genes_of_interest_fdr_Lt_pointOne.txt", "rU") as genes:
		for i in genes.readlines():
			LOCs.append(i.strip())
	with open("../ExternalDataSets/GOtermsFull.txt", "rU") as get_go:
		for line in get_go.readlines():
			data = line.split("\t")
			if data[0] in LOCs and data[1].strip() !=  "all":
				GO.append(data[1].strip())
	#print(GO)
	return GO
				

#def GO_annotation():

def Add_tildas(l):
	m = []
	for i in l:
		m.append(str(i).replace(" ", "~"))
		
	return m

def Main():
	func = []
	GO = Query()
	print (len(GO))
	#GO_data = GO_annotation()
	with open("../ExternalDataSets/gene_association.gramene_oryza", "rU") as file:
		for line in file.readlines():
			if line[0] != "!":
				toks = line.rstrip().split('\t')
				#print (toks)
				for g in GO:
					if str(toks[4]) == str(g):
						func.append(toks[9])
						GO.pop(GO.index(g))
						print ("GO len: " + str(len(GO)))
	func = Add_tildas(func)
	with open("output.txt", "w") as output:
		for i in func:
			output.write(str(i) + "\n")
			#break
	#print (len(func))

Main()		
