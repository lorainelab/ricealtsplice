#Script to access if all of our target gene notations can be found in gene_association.gramene_oryza


def Check_for_all_locs():
    goi_count = 0 #Genes Of Interest counter
    ref_LOCs_count = 0 #the target locs found in the gene_association.gramene_oryza file
    #q is our unique genes of interset
    q = ['LOC_Os01g01340', 'LOC_Os01g07810', 'LOC_Os01g10400', 'LOC_Os01g14860', 'LOC_Os01g16870', 'LOC_Os01g18800', 'LOC_Os01g25330', 'LOC_Os01g25484', 'LOC_Os01g35580', 'LOC_Os01g43390', 'LOC_Os01g45274', 'LOC_Os01g51200', 'LOC_Os01g51290', 'LOC_Os01g55570', 'LOC_Os01g61670', 'LOC_Os01g63270', 'LOC_Os01g65080', 'LOC_Os01g71590', 'LOC_Os01g73910', 'LOC_Os02g01150', 'LOC_Os02g03070', 'LOC_Os02g05510', 'LOC_Os02g05830', 'LOC_Os02g13560', 'LOC_Os02g39790', 'LOC_Os02g40514', 'LOC_Os02g47400', 'LOC_Os02g57280', 'LOC_Os02g57290', 'LOC_Os03g02690', 'LOC_Os03g05390', 'LOC_Os03g10080', 'LOC_Os03g11230', 'LOC_Os03g11550', 'LOC_Os03g14669', 'LOC_Os03g15930', 'LOC_Os03g22270', 'LOC_Os03g28330', 'LOC_Os03g37640', 'LOC_Os03g52239', 'LOC_Os03g57660', 'LOC_Os03g61990', 'LOC_Os04g35380', 'LOC_Os04g38870', 'LOC_Os04g41470', 'LOC_Os04g44890', 'LOC_Os04g55850', 'LOC_Os04g56110', 'LOC_Os04g57210', 'LOC_Os05g02780', 'LOC_Os05g06450', 'LOC_Os05g25840', 'LOC_Os05g29760', 'LOC_Os05g38040', 'LOC_Os05g41460', 'LOC_Os05g48040', 'LOC_Os05g48800', 'LOC_Os05g49410', 'LOC_Os05g51420', 'LOC_Os06g03860', 'LOC_Os06g05110', 'LOC_Os06g08530', 'LOC_Os06g14370', 'LOC_Os06g22380', 'LOC_Os06g24730', 'LOC_Os06g36360', 'LOC_Os06g51220', 'LOC_Os07g33350', 'LOC_Os07g36180', 'LOC_Os08g04440', 'LOC_Os08g09250', 'LOC_Os08g33380', 'LOC_Os08g36150', 'LOC_Os08g44860', 'LOC_Os09g10710', 'LOC_Os09g23550', 'LOC_Os09g27660', 'LOC_Os10g33800', 'LOC_Os10g36000', 'LOC_Os10g40410', 'LOC_Os11g34460', 'LOC_Os12g08260']
    ref_locs = [] #will populate with genes from gene_association.gramene_oryza
    with open("gene_association.gramene_oryza", "rU") as file:
            reg1=re.compile(r'LOC_Os..g\d{5}')
            for line in file.readlines():
                genes=reg1.findall(line)
                for i in genes:
                        ref_locs.append(str(i)) #makes a list of all the genes found in gene_association.gramene_oryza
    
    for i in q: #checking our query, q, against ref_locs
        goi_count += 1
        if str(i) in ref_locs:
            ref_LOCs_count += 1
    print ("GOI: {0}, Ref: {1}".format(goi_count, ref_LOCs_count))
    
Check_for_all_locs()