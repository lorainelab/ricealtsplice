Creating RPM and RPKM files
===========================

Introduction
------------
The goal of this file is to create an RPM (counts per million) file and RPKM (counts per million thousand) with expression data for every annotated gene.

The RPM file is useful for looking at expression across samples and the RPKM file is useful for looking at expression between genes within the same sample. 

Questions:

* How many genes had zero counts?

Methods
-------

Counts per gene were generated previously - using featureCounts. We'll use the cpm method from edgeR Bioconductor package to calculate counts per million.

Get counts per gene:

```{r}
source("../src/RiceAltspliceFunctions.R")
fname="data/MSU7.featureCounts.txt.gz"
counts=getCounts(fname)
```

* How many genes had zero counts?

```{r}
zeros=sum(rowSums(counts)==0)
```

* There were `r zeros` genes with zero counts in any sample. This was `r round(zeros/nrow(counts)*100,1)`% of all rice genes.

Load edgeR library and use the cpm function to caculated normalized expression values per gene:

```{r}
suppressPackageStartupMessages(library(edgeR))
cpms=data.frame(cpm(counts))
```

Read functional annotations:

```{r}
annots=getMsu7Annots()
```

Merge counts and annotations and write file:

```{r}
counts$gene_id=row.names(counts)
d=merge(annots,counts,by.x="gene_id",by.y="gene_id")
fname="results/count.txt"
write.table(d,file=fname,row.names=F,sep='\t',quote=F)
system(paste('gzip -f',fname))
```

Merge cpm and annotations:

```{r}
cpms$gene_id=row.names(cpms)
d=merge(annots,cpms,by.x='gene_id',by.y='gene_id')
```

Write data file:

```{r}
f=paste0('results/RPM.txt')
write.table(d,file=f,row.names=F,sep='\t',quote=F)
system(paste('gzip -f',f))
```

Make RPKM file
--------------

Get transcript sizes:

```{r}
sizes=getTxSizes()
sizes = sizes[d$gene_id]
```

Divide expression by largest transcript size:

```{r}
v=names(d)[grep('\\d',names(d))]
for (sample in v) {
  d[,sample]=d[,sample]/sizes
}
```

Write file:

```{r}
f=paste0('results/RPKM.txt')
write.table(d,file=f,row.names=F,sep='\t',quote=F)
system(paste('gzip -f',f))
```

Conclusion
==========

We wrote RPM and RPKM data files needed for other analyses.

There was a surprising number of rice genes with no detectable expression in the data set: `r round(zeros/nrow(counts)*100,1)`% of all rice genes.

Limitations
===========

The method for calculating RPM is flawed. The cpm method gets the number of mapped reads from the raw counts data frame, which is problematic because

* some reads from overlapping genes are counted more than once
* counts for reads outside gene regions are not included

However, we are doing it this way (using cpm) to achieve consistency with the differential expression analysis. 

Session information
===================

```{r}
sessionInfo()
````

