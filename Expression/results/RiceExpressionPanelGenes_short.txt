ExpressionCategory	gene	averageRPKM	percentSD	shortestTranscriptSize	longestTranscriptSize	numModels	diffFromCategoryMid	description
high	LOC_Os04g11880	29.945510184502	6.86319496498444	3118	3394	3	0.0122028303160917	WD domain, G-beta repeat domain containing protein, expressed
high	LOC_Os12g38110	30.0870640905618	1.7922595284157	4683	4683	1	0.153756736375925	importin subunit beta, putative, expressed
high	LOC_Os01g59980	30.1591916129998	2.50459639141313	3246	3246	1	0.225884258813888	zinc finger family protein, putative, expressed
high	LOC_Os03g16440	29.7048372525052	3.2807728978813	3233	3233	1	0.22847010168071	outer membrane protein, OMP85 family, putative, expressed
high	LOC_Os11g36470	29.6720853245529	7.70058741347247	3463	3463	1	0.261222029633043	ubiquitin carboxyl-terminal hydrolase 21, putative, expressed
low	LOC_Os10g17610	0.113230048405794	16.8446151782971	4104	4104	1	0.00162247214288416	retrotransposon protein, putative, unclassified, expressed
low	LOC_Os04g53060	0.108632953468158	16.8713633899893	4469	4469	1	0.00297462279475232	NBS-LRR disease resistance protein, putative, expressed
low	LOC_Os12g06270	0.108234895027642	8.39633569435399	3802	3802	1	0.00337268123526797	retrotransposon protein, putative, unclassified, expressed
low	LOC_Os01g12600	0.115731079572005	17.744439016318	3376	3376	1	0.00412350330909472	retrotransposon protein, putative, unclassified, expressed
low	LOC_Os02g20420	0.115867588104339	22.0334089243887	6192	6192	1	0.00426001184142846	retrotransposon protein, putative, LINE subclass, expressed
moderate	LOC_Os04g44750	0.825113112872697	9.90443252173722	3052	3052	1	0.00189196670273295	transporter family protein, putative, expressed
moderate	LOC_Os11g35410	0.817103610659129	8.62130763431763	3316	3316	1	0.00611753551083583	expressed protein
moderate	LOC_Os04g39780	0.81270340384814	8.6373858780312	7452	7452	1	0.0105177423218245	AMP-binding enzyme family protein, expressed
moderate	LOC_Os03g01240	0.811851865243705	4.39675976810819	6314	6314	1	0.0113692809262598	CCR4-Not complex component, Not1domain containing protein, expressed
moderate	LOC_Os02g22570	0.837809205747798	4.34913926332594	4521	4521	1	0.0145880595778335	retrotransposon protein, putative, unclassified, expressed
moderate-high	LOC_Os05g40050	6.07426649254441	6.58007445043154	3044	3094	2	0.00216029515713423	receptor-like protein kinase 2 precursor, putative, expressed
moderate-high	LOC_Os01g19870	6.06711303271195	6.07402265945074	3204	3204	1	0.00499316467532296	hydrolase, acting on carbon-nitrogen, putative, expressed
moderate-high	LOC_Os04g44710	6.06585565320927	4.817792360883	3553	3668	2	0.00625054417800275	double-stranded RNA binding motif containing protein, expressed
moderate-high	LOC_Os03g02110	6.04995298252876	8.17672357466746	4846	4851	2	0.0221532148585197	WD domain, G-beta repeat domain containing protein, expressed
moderate-high	LOC_Os05g34220	6.09910334622248	2.31372217881926	3519	3728	2	0.0269971488352034	vrga1, putative, expressed
very high	LOC_Os03g06240	147.456747031636	1.62326078180566	3149	3152	2	0.103730343692661	YT521-B-like family domain containing protein, expressed
very high	LOC_Os06g08530	146.925474319023	3.70364021521712	3149	3697	5	0.635003056305976	ubiquitin carboxyl-terminal hydrolase domain containing protein, expressed
very high	LOC_Os08g44860	138.000740710871	5.3258484486355	3222	3544	8	9.55973666445766	aminopeptidase, putative, expressed
very high	LOC_Os09g36300	134.678336623977	4.59871053213159	3070	3433	4	12.8821407513517	OsLonP4 - Putative Lon protease homologue, expressed
very high	LOC_Os06g09450	162.914274382216	3.65505206108674	3122	3816	8	15.353797006887	sucrose synthase, putative, expressed
